/* This script assumes the db2 instance has already been created and users added. If that is not the case, check:

https://www.ibm.com/support/knowledgecenter/SSLKT6_7.6.0/com.ibm.mam.doc/pdf_mam_install_was.pdf - pag. 35 

It also assumes the script will be run as user ctginst1
*/

/* Create db2 database */
db2 create db ’maxdb76’ ALIAS ’maxdb76’ using codeset UTF-8 territory US pagesize 32 K
db2 connect to ’maxdb76’
db2 GRANT DBADM ON DATABASE TO USER db2admin (windows only)
db2 GRANT SECADM ON DATABASE TO USER db2admin (windows only)
db2 connect reset

/* Configure the db2 database */
db2 update db cfg for maxdb76 using SELF_TUNING_MEM ON
db2 update db cfg for maxdb76 using APPGROUP_MEM_SZ 16384 DEFERRED
db2 update db cfg for maxdb76 using APPLHEAPSZ 2048 AUTOMATIC DEFERRED
db2 update db cfg for maxdb76 using AUTO_MAINT ON DEFERRED
db2 update db cfg for maxdb76 using AUTO_TBL_MAINT ON DEFERRED
db2 update db cfg for maxdb76 using AUTO_RUNSTATS ON DEFERRED
db2 update db cfg for maxdb76 using AUTO_REORG ON DEFERRED
db2 update db cfg for maxdb76 using AUTO_DB_BACKUP ON DEFERRED
db2 update db cfg for maxdb76 using CATALOGCACHE_SZ 800 DEFERRED
db2 update db cfg for maxdb76 using CHNGPGS_THRESH 40 DEFERRED
db2 update db cfg for maxdb76 using DBHEAP AUTOMATIC
db2 update db cfg for maxdb76 using LOCKLIST AUTOMATIC DEFERRED
db2 update db cfg for maxdb76 using LOGBUFSZ 1024 DEFERRED
db2 update db cfg for maxdb76 using LOCKTIMEOUT 300 DEFERRED
db2 update db cfg for maxdb76 using LOGPRIMARY 50 DEFERRED
db2 update db cfg for maxdb76 using LOGSECOND 100 DEFERRED
db2 update db cfg for maxdb76 using LOGFILSIZ 8192 DEFERRED
db2 update db cfg for maxdb76 using SOFTMAX 1000 DEFERRED
db2 update db cfg for maxdb76 using MAXFILOP 30720 DEFERRED #32-bit UNIX
db2 update db cfg for maxdb76 using MAXFILOP 61440 DEFERRED #64-bit UNIX
db2 update db cfg for maxdb76 using PCKCACHESZ AUTOMATIC DEFERRED
db2 update db cfg for maxdb76 using STAT_HEAP_SZ AUTOMATIC DEFERRED
db2 update db cfg for maxdb76 using STMTHEAP AUTOMATIC DEFERRED
db2 update db cfg for maxdb76 using UTIL_HEAP_SZ 10000 DEFERRED
db2 update db cfg for maxdb76 using DATABASE_MEMORY AUTOMATIC DEFERRED
db2 update db cfg for maxdb76 using AUTO_STMT_STATS OFF DEFERRED
db2 update db cfg for maxdb76 using STMT_CONC LITERALS DEFERRED
db2 update alert cfg for database on maxdb76 using db.db_backup_req SET THRESHOLDSCHECKED YES
db2 update alert cfg for database on maxdb76 using db.tb_reorg_req SET THRESHOLDSCHECKED YES
db2 update alert cfg for database on maxdb76 using db.tb_runstats_req SET THRESHOLDSCHECKED YES
db2 update dbm cfg using PRIV_MEM_THRESH 32767 DEFERRED
db2 update dbm cfg using KEEPFENCED NO DEFERRED
db2 update dbm cfg using NUMDB 2 DEFERRED
db2 update dbm cfg using RQRIOBLK 65535 DEFERRED
db2 update dbm cfg using HEALTH_MON OFF DEFERRED
db2 update dbm cfg using AGENT_STACK_SZ 1000 DEFERRED
db2 update dbm cfg using MON_HEAP_SZ AUTOMATIC DEFERRED
db2set DB2_SKIPINSERTED=ON
db2set DB2_INLIST_TO_NLJN=YES
db2set DB2_MINIMIZE_LISTPREFETCH=Y
db2set DB2_EVALUNCOMMITTED=YES
db2set DB2_FMP_COMM_HEAPSZ=65536
db2set DB2_SKIPDELETED=ON
db2set DB2_USE_ALTERNATE_PAGE_CLEANING=ON

/* Restart db2 */
db2stop force
db2start
db2 connect to ’maxdb76’

/* Create a buffer pool */
db2 CREATE BUFFERPOOL MAXBUFPOOL IMMEDIATE SIZE 4096 AUTOMATIC PAGESIZE 32 K

/* Create table spaces. */
db2 CREATE REGULAR TABLESPACE MAXDATA PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE INITIALSIZE 5000 M BUFFERPOOL MAXBUFPOOL
db2 CREATE TEMPORARY TABLESPACE MAXTEMP PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE BUFFERPOOL MAXBUFPOOL
db2 CREATE REGULAR TABLESPACE MAXINDEX PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE INITIALSIZE 5000 M BUFFERPOOL MAXBUFPOOL
db2 GRANT USE OF TABLESPACE MAXDATA TO USER MAXIMO

/* Create Schema */
db2 create schema maximo authorization maximo

/* Grant authority to maximo. */
db2 GRANT DBADM,CREATETAB,BINDADD,CONNECT,CREATE_NOT_FENCED_ROUTINE,IMPLICIT_SCHEMA,LOAD,CREATE_EXTERNAL_ROUTINE,QUIESCE_CONNECT,SECADM ON DATABASE TO USER MAXIMO
db2 GRANT USE OF TABLESPACE MAXDATA TO USER MAXIMO
db2 GRANT CREATEIN,DROPIN,ALTERIN ON SCHEMA MAXIMO TO USER MAXIMO

/* Break the database connection. */
db2 connect reset

/* Set DB2 compatibility with Oracle  */
db2set DB2_COMPATIBILITY_VECTOR=ORA
db2stop FORCE
db2start

/* Create new syscat.packages to solve issue #44  */
bind @db2cli.lst CLIPKG 10 grant public blocking all