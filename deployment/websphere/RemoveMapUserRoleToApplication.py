role="maximouser"
groups="maximousers"
application="MAXIMO"

roleMapping = [role, "no", "no", "", groups]
print "==> Removing Mapping " + application + " " + role + " role"
AdminApp.edit(application, ["-MapRolesToUsers", [roleMapping]])
    
# Commit changes to the configuration repository 
AdminConfig.save()