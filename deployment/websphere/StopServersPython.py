servers = [ 
    {'nodename':'DC2AP870Node01',    'servername':'webserver1'}, 
    {'nodename':'DC2AP870Node01',    'servername':'server1'}
]

execfile('D:/maximo/deployment/websphere/wsadminlib.py')
  
webservers = listServersOfType('WEB_SERVER')
 
for i in range(0,5):
    try:
        # stop application servers
        for s in servers:
            if [s['nodename'], s['servername']] not in webservers:
                print 'Stopping server ' + s['servername'] + ' attempt '+str(i)
                stopServer(s['nodename'], s['servername'])

    except Exception:
        continue
    break
