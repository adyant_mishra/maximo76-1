package psdi.gbsassets.en;

import java.io.PrintStream;
import java.sql.Connection;
import java.util.HashMap;

import com.ibm.gbs.gbsassets.en.GBSAutoUpgradeTemplate;

/**
 * Load automation scripts
 * 
 * @author ibm gbs
 */

public class GBSAutomationScriptDeploy extends GBSAutoUpgradeTemplate {

	public GBSAutomationScriptDeploy(Connection paramConnection) throws Exception {
		super(paramConnection);
	}

	public GBSAutomationScriptDeploy(Connection paramConnection, PrintStream paramPrintStream) throws Exception {
		super(paramConnection, paramPrintStream);
	}

	public GBSAutomationScriptDeploy(Connection paramConnection, HashMap<?,?> paramHashMap, PrintStream paramPrintStream) throws Exception {
		super(paramConnection, paramHashMap, paramPrintStream);
	}
	
	protected void init() throws Exception
	{
		super.init();
		this.scriptFileName = "GBSAutomationScriptDeploy"; 
	}
	
	/**
	 * Runs DBConfig to apply changes & Apply Automation Scripts
	 */
	@Override
	protected void process() throws Exception
	{
		super.process();
		//Loading Automation Scripts
		loadAutomationScripts();
	}	

}
