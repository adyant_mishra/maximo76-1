package com.ibm.gbs.gbsassets.en;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GBSDBCSQLExport {
	
	static DateFormat format = new SimpleDateFormat("yyMMdd-HHmmss");
	static boolean createDeletesFlag = false;

	public static void main(String[] args) throws Exception {
		String propFileDir = null;
		String propFileName = null;
		int exportType = Integer.parseInt(args[0]);
		String dbtype = args[1];
		String outputFolder = args[2];
		String createDeletes = args[3];
		
		if (outputFolder == null || outputFolder.trim().equals("")) {
			outputFolder = "GBSDBCSql";
		}
		File outputFolderFile = new File(outputFolder);
		if (createDeletes.equalsIgnoreCase("y")) {
			createDeletesFlag = true;
		}
		
		String object = args[4];
		String where = args[5];
		if(where == null || where.trim().equals("")) {
			where = "1=1";
		}
		
		System.out.println("Args: "+ String.valueOf(args.length));
		if (args.length > 6)
		{			
			propFileDir = args[6];		
			propFileName = args[7];
		}
		String hostname = "Unknown";
		try
		{
		    InetAddress addr;
		    addr = InetAddress.getLocalHost();
		    hostname = addr.getHostName();
		}
		catch (UnknownHostException ex)
		{
		    System.out.println("Hostname can not be resolved");
		}

		Connection conn = GBSDBConnectionManager.getConnection(propFileDir, propFileName);
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("--").append("\n");
		buffer.append("-- Object:   ").append(object).append("\n");
		buffer.append("-- Where:    ").append(where).append("\n");
		buffer.append("-- Date:     ").append(new Date()).append("\n");
		buffer.append("-- Host:     ").append(hostname).append("\n");
		buffer.append("-- Database: ").append(conn.getMetaData().getURL()).append("\n");
		buffer.append("--").append("\n");
		GBSDBUtil.processInsertToBuffer(buffer, object, where, conn, dbtype, createDeletesFlag);
		
		if(!outputFolderFile.exists()) {
			outputFolderFile.mkdirs();
		}
		File file = new File(outputFolderFile, object.toLowerCase() + format.format(new Date()) + ".sql");
		
		GBSDBCSQLExport.writeToFile(file, buffer);
		
		System.exit(0);
	}
	
	private static void writeToFile(File file, StringBuffer data) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
		out.write(data.toString());
		out.flush();
		out.close();
	}
}
