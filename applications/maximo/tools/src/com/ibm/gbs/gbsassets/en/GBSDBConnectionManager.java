package com.ibm.gbs.gbsassets.en;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.util.Properties;
import com.ibm.tivoli.maximo.dbmanage.ScriptDBConnect;

import psdi.util.MXProperties;
import psdi.util.MXSystemException;

/**
 * @author caiosaad
 * Tool to create and maintain database connection 
 * 
 * Change History
 * 
 * Name							Reason			Description
 * --------------------------------------------------------------
 * simon.richards@au1.ibm.com	DataLoad		Updates to DBCOnnection tool
 *											 	Updates to use property file
 */ 

public class GBSDBConnectionManager {

	private static Connection conn = null;
	/**
	 * properties file
	 */
	static Properties properties = null;

	public static Connection getConnection(String propertiesFileLocation, String propertiesFileName)
	{
		String propDir;
		String propFileName;

		if (conn != null)
		{
			System.out.println("Returning Connection: "+ conn.toString());
			return conn;
		}
		
		if (propertiesFileName == null)
			propFileName = "maximo.properties";
		else
			propFileName = propertiesFileName;

		if (propertiesFileLocation == null)
		{
			propDir = ".." + java.io.File.separator + ".."
				+ java.io.File.separator + "applications"
				+ java.io.File.separator + "maximo" + java.io.File.separator
				+ "properties";
		}
		else
			propDir = propertiesFileLocation;

		File propFile = new File(propDir, propFileName) ;
		if (! propFile.exists())
		{
			System.out.println("Does not exist: " + propFile.getAbsolutePath()) ;
		}
		
		FileInputStream fis;
		
		try 
		{
			fis = new FileInputStream(propDir + File.separator + propFileName);
			properties = MXProperties.loadProperties(fis, false);
		} 
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		catch (MXSystemException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		try 
		{
			conn = ScriptDBConnect.getConnection(properties) ;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("Returning Connection: "+ conn.toString());
		return conn;

	}

}
