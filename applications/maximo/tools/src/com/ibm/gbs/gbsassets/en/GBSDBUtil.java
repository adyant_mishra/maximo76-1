package com.ibm.gbs.gbsassets.en;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

public class GBSDBUtil {
	
	public static String replaceVariables(LinkedHashMap<String, Object> record, String sql) {
		
		for (String columnName : record.keySet()) {
			String var = "{" + columnName + "}";
			sql = sql.replace(var, (String) record.get(columnName));			
		}		
		return sql;
	}
	
	public static void process(FileWriter out, String tableName, String keyName, String keyValue, Connection conn, String dbtype, boolean createDeletesFlag, String additionalWhereClause) throws Exception{
		String selectQuery = "select * from "+tableName+" where "+keyName+" = '"+keyValue+"'";
		
		if (additionalWhereClause != null) selectQuery += " " + additionalWhereClause;
		
		selectQuery += " order by "+keyName;
		
		System.out.println("Select Query: "+ selectQuery);
		ResultSet rs = conn.createStatement().executeQuery(selectQuery);

		boolean firstRun = true;

		while (rs.next()){
			LinkedHashMap<String, Object> record = extractRecord(rs);

			System.out.println("KeyName: "+keyName);
			System.out.println("KeyValue: "+keyValue);
			System.out.println("Record: "+ record.toString());
			System.out.println("Value: "+(String)record.get(keyName.toLowerCase()));

			if (firstRun && createDeletesFlag)
			{
				firstRun = false;
				String delete = "delete from "+tableName+" where "+keyName+" = '"+(String)record.get(keyName.toLowerCase())+"'";
				if (additionalWhereClause != null) delete += " " + additionalWhereClause;
				out.write("\n" + delete + ";\n");
			}

			out.write(generateInsert(tableName,record,dbtype).replace("'N-U-L-L'", "null")+"\n");
		}
		System.out.println("Processing of table '"+tableName+"' completed.");		
	}
	
	public static void process(FileWriter out, String tableName, String keyName, String keyValue, Connection conn, String dbtype, boolean createDeletesFlag) throws Exception{
		process(out, tableName, keyName, keyValue, conn, dbtype, createDeletesFlag, null);
	}

	/**
	 * Additional functionality to generate insert scripts to a buffer. 
	 * The starting point is the process method of this same class. 
	 * 
	 * @author unknown/jwmarsden@au1.ibm.com
	 */
	public static void processInsertToBuffer(StringBuffer buffer, String tableName, String keyName, String keyValue, Connection conn, String dbtype, boolean createDeletesFlag) throws Exception{
		String selectQuery = "select * from "+tableName+" where "+keyName+" = '"+keyValue+"' order by "+keyName;
		System.out.println("Select Query: "+ selectQuery);
		ResultSet rs = conn.createStatement().executeQuery(selectQuery);
		boolean firstRun = true;
		while (rs.next()){
			LinkedHashMap<String , Object> record = extractRecord(rs);
			if (firstRun && createDeletesFlag)
			{
				firstRun = false;
				buffer.append("\ndelete from "+tableName+" where "+keyName+" = '"+(String)record.get(keyName.toLowerCase())+"';\n");
			}
			String insert = generateInsert(tableName,record,dbtype);
			insert = insert.replace("'N-U-L-L'", "null");
			
			switch (dbtype)
			{
				case "1": case "MSSQL":
					insert = insert.replace("\r", "' + CHAR(13) + '\r");
					insert = insert.replace("\n", "' + CHAR(10) + '\n");
					break;	
					
				case "2": case "ORACLE":
					insert = insert.replace("\r", "");
					insert = insert.replace("\n", "");
					break;
			}
			buffer.append(insert+"\n");
		}
		System.out.println("Processing of table '"+tableName+"' completed.");
	}
	
	public static void processInsertToBuffer(StringBuffer buffer, String tableName, String where, Connection conn, String dbtype, boolean createDeletesFlag) throws Exception{
		String selectQuery = "select * from " + tableName;
		if (where != null && !where.trim().equals("1=1") && !where.trim().equals("1 = 1")) {
			selectQuery += (where.toLowerCase().contains("where") ? "" : " where") + " " + where;
		}
		System.out.println("Select Query: "+ selectQuery);
		ResultSet rs = conn.createStatement().executeQuery(selectQuery);
		boolean firstRun = true;
		while (rs.next()){
			LinkedHashMap<String , Object> record = extractRecord(rs);
			if (firstRun && createDeletesFlag)
			{
				firstRun = false;
				buffer.append("\ndelete from "+tableName); 
				if (where != null && !where.trim().equals("1=1") && !where.trim().equals("1 = 1")) {
					buffer.append((where.toLowerCase().contains("where") ? "" : " where") + " " + where);
				}
				buffer.append(";").append('\r').append('\n');
			}
			String insert = generateInsert(tableName,record,dbtype);
			insert = insert.replace("'N-U-L-L'", "null");
			
			switch (dbtype)
			{
				case "1": case "MSSQL":
					insert = insert.replace("\r", "' + CHAR(13) + '\r");
					insert = insert.replace("\n", "' + CHAR(10) + '\n");
					break;	
					
				case "2": case "ORACLE":
					insert = insert.replace("\r", "");
					insert = insert.replace("\n", "");
					break;
			}
			buffer.append(insert+"\n");
		}
		System.out.println("Processing of table '"+tableName+"' completed.");
	}
	
	public static void processUpdateToBuffer(StringBuffer buffer, String tableName, Map<String , String> recordKeys, String[] updateColumns, Connection conn, String dbtype) throws Exception{
		StringBuilder selectBuilder = new StringBuilder();
		boolean firstIteration = true;
		selectBuilder.append("select ");
		for(String updateColumn : updateColumns) {
			if (firstIteration)
				firstIteration = false;
			else
				selectBuilder.append(", ");
			selectBuilder.append(updateColumn);
		}
		selectBuilder.append(" from " + tableName);
		selectBuilder.append(" where ");
		firstIteration = true;
		for (String key : recordKeys.keySet()){
			if (firstIteration)
				firstIteration = false;
			else
				selectBuilder.append(" AND ");
			selectBuilder.append(key);
			selectBuilder.append("=");
			selectBuilder.append(format(recordKeys.get(key),dbtype));
		}
		String selectQuery = selectBuilder.toString();
		System.out.println("Select Query: "+ selectQuery);
		ResultSet rs = conn.createStatement().executeQuery(selectQuery);
		while (rs.next()){
			LinkedHashMap<String , Object> record = extractRecord(rs);


			String update = generateUpdate(tableName,recordKeys,record,dbtype);
			update = update.replace("'N-U-L-L'", "null");
			switch (dbtype)
			{
				case "1": case "MSSQL":
					update = update.replace("\r", "' + CHAR(13) + '\r");
					update = update.replace("\n", "' + CHAR(10) + '\n");
					break;	
					
				case "2": case "ORACLE":
					update = update.replace("\r", "");
					update = update.replace("\n", "");
					break;
			}
			buffer.append(update+"\n");
		}
		System.out.println("Processing of table '"+tableName+"' completed.");
	}
	
	public static LinkedHashMap<String, Object> extractRecord(ResultSet rs) throws Exception{

		LinkedHashMap<String, Object> record = new LinkedHashMap<String, Object>();

		ResultSetMetaData metadata = rs.getMetaData(); 
		int colNum = metadata.getColumnCount();

		for (int i=1; i<=colNum; i++){

			String columnName = metadata.getColumnName(i);
			Object columnValue = rs.getObject(i);

			System.out.println("ColumnName: "+columnName);			

			if (columnValue == null)
				columnValue = "N-U-L-L";

			if (columnValue instanceof String)
			{
				columnValue = ((String)columnValue).replace("'", "''");
				System.out.println("ColumnValue: "+columnValue);
			}


			record.put(columnName, columnValue);
		}

		return record;
	}

	public static String generateInsert(String tableName, LinkedHashMap<String, Object> record, String dbtype){

		StringBuilder insertSQL = new StringBuilder();  
		//String ignoredColumns[] = new String[]{tableName+"ID", "ROWSTAMP"};
		String ignoredColumns[] = new String[]{"ROWSTAMP"};

		insertSQL.append("insert into "+tableName+ " (");

		boolean firstIteration = true;
		outer:
			for (String columnName : record.keySet()){

				for (String columnToIgnore : ignoredColumns)
					if (columnName.equalsIgnoreCase(columnToIgnore) && !columnName.equals(tableName.replace("CFG", "")+"ID"))
						continue outer;

				if (firstIteration)
					firstIteration = false;
				else
					insertSQL.append(", ");

				insertSQL.append(columnName);
			}

		insertSQL.append(") values (");

		firstIteration = true;
		outer2:
			for (String columnName : record.keySet()){

				for (String columnToIgnore : ignoredColumns)
					if (columnName.equalsIgnoreCase(columnToIgnore) && !columnName.equals(tableName.replace("CFG", "")+"ID"))
						continue outer2;

				if (firstIteration)
					firstIteration = false;
				else
					insertSQL.append(", ");

				if (columnName.equalsIgnoreCase(tableName.replace("CFG", "")+"ID"))
					insertSQL.append(tableName+"SEQ.NEXTVAL");
				else
					insertSQL.append(format(record.get(columnName),dbtype));
			}

		insertSQL.append(");");

		return insertSQL.toString();
	}
	
	public static String generateUpdate(String tableName, Map<String, String> record, Map<String, Object> updates, String dbtype){
		StringBuilder updateSQL = new StringBuilder();  
		updateSQL.append("update "+tableName+ " set ");

		boolean firstIteration = true;
		for (String columnName : updates.keySet()){
			if (firstIteration)
				firstIteration = false;
			else
				updateSQL.append(", ");
			updateSQL.append(columnName);
			updateSQL.append("=");
			updateSQL.append(format(updates.get(columnName),dbtype));
		}
		firstIteration = true;
		updateSQL.append(" where ");
		for (String columnName : record.keySet()){
			if (firstIteration)
				firstIteration = false;
			else
				updateSQL.append(" AND ");
			updateSQL.append(columnName);
			updateSQL.append("=");
			updateSQL.append(format(record.get(columnName),dbtype));
		}
		updateSQL.append(";");
		return updateSQL.toString();
	}

	public static Object format(Object record, String dbtype){

		if (record instanceof Timestamp) {

			String formattedDate = "";

			switch (dbtype)
			{
				case "1": case "MSSQL":
					formattedDate = "'"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(record)+"'";
					break;	
					
				case "2": case "ORACLE":
					String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(record);
					formattedDate =  "TO_DATE('"+timestamp+"','YYYY-MM-DD HH24:MI:SS')";
					break;
			}

			return formattedDate;

		} else if (record instanceof Number) {
			return ""+record;
		}
		

		return "'"+record+"'";
	}
}
