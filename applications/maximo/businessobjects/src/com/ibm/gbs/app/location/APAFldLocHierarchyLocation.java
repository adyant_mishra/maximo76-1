package com.ibm.gbs.app.location;

 
import java.rmi.RemoteException;

import psdi.app.location.FldLocHierarchyLocation;
import psdi.app.location.FldLocation;
import psdi.app.location.LocHierarchy;
import psdi.app.location.LocSystemRemote;
import psdi.app.location.LocationRemote;
import psdi.app.location.LocationServiceRemote;
import psdi.mbo.Mbo;
import psdi.mbo.MboRemote;
import psdi.mbo.MboSetRemote;
import psdi.mbo.MboValue;
import psdi.security.UserInfo;
import psdi.server.MXServer;
import psdi.util.MXApplicationException;
import psdi.util.MXException;



public class APAFldLocHierarchyLocation
  extends FldLocation
{
  public APAFldLocHierarchyLocation(MboValue mbv)
    throws MXException
  {
    super(mbv, "OPERATING");
  }
  
  public void validate()
    throws MXException, RemoteException
  {
    super.validate();
    MboValue mv = getMboValue();
    LocHierarchy locH = (LocHierarchy)mv.getMbo();
    String location = mv.getString();
    String parent = locH.getString("parent");
    if (location.equals(parent)) {
      throw new MXApplicationException("locations", "selfChildSameValue");
    }
    if (!locH.getMboSet("LOCHIERARCHY").isEmpty()) {
      throw new MXApplicationException("locations", "relationExists");
    }
    MboRemote locSys = locH.getMboSet("LOCSYSTEM").getMbo(0);
    locSys.getThisMboSet().reset();
    if (locSys == null) {
      throw new MXApplicationException("locations", "noSystemDefined");
    }
    MboRemote owner = locH.getOwner();
    
    MboSetRemote lochierset = null;
    if (locSys.getBoolean("network"))
    {
      if ((owner instanceof LocSystemRemote)) {
        lochierset = owner.getMboSet("LOCHIERARCHY_PARENTS");
      } else if ((owner instanceof LocationRemote)) {
        lochierset = owner.getMboSet("LOCHIERARCHY");
      }
      if ((lochierset != null) && (!lochierset.isEmpty()))
      {
        MboRemote locHier = null;
        int index = 0;
        while ((locHier = lochierset.getMbo(index)) != null)
        {
          if (locHier.getString("parent").equals(location))
          {
            Object[] params = { location, parent, getMboValue("systemid").getString() };
            throw new MXApplicationException("locations", "parentExists", params);
          }
          index++;
        }
      }
      return;
    }
    MboSetRemote locAncSet = locH.getMboSet("PARENTASDESCENDANT");
    MboRemote locAnc = null;
    int index = 0;
    while ((locAnc = locAncSet.getMbo(index)) != null)
    {
      if (locAnc.getString("ancestor").equals(location))
      {
        Object[] params = { location, parent, getMboValue("systemid").getString() };
        throw new MXApplicationException("locations", "parentExists", params);
      }
      index++;
    }
    if (locSys.getBoolean("network")) {
      return;
    }
    MboSetRemote inSystem = locH.getMboSet("INSYSTEM");
    inSystem.reset();
    if (!inSystem.isEmpty())
    {
      LocationServiceRemote lsr = (LocationServiceRemote)MXServer.getMXServer().lookup("LOCATION");
      String primary = lsr.getPrimarySystem(locH.getUserInfo(), locH.getString("siteid"));
      if (primary.equals(locH.getString("systemid"))) {
        throw new MXApplicationException("locations", "invalidprocess");
      }
      Object[] param = { getMboValue().getString(), locH.getString("systemid") };
      throw new MXApplicationException("locations", "mayMakeNetwork", param);
    }
  }
  
  String relationWhere = null;
  
  public MboSetRemote getList()
    throws MXException, RemoteException
  {
    MboRemote mboRemote = getMboValue().getMbo();
    
    
    boolean found = false;
    boolean isRepairFacility = false;
    MboRemote owner = mboRemote.getOwner();
    String name =null;
    if (owner != null)
    {
      owner = owner.getOwner();
     
      name=owner.getName();
    		  
       //Removing repairfacility assignment so that it can be used across different apps other than work order.
      
      if (owner != null & !(name.equalsIgnoreCase("WORKORDER")))
      {
        //isRepairFacility = owner.getBoolean("isrepairfacility");
        found = true;
      } 
    }
    String sqlWhere = null;
    if ((mboRemote.isZombie()) || (mboRemote.getString("parent").equalsIgnoreCase("")))
    {
      sqlWhere = " and location is not null";
    }
    else
    {
      sqlWhere = " and location != :parent";
      
      //Commented out as this attribute might be required to be created in other objects.
      
      /**
      if (found) {
        if (isRepairFacility) {
          sqlWhere = sqlWhere + " and isrepairfacility = 1";
        } else {
          sqlWhere = sqlWhere + " and isrepairfacility = 0";
        }
      }
      **/
    }
    MboSetRemote locationSet = super.getList();
    if (this.relationWhere == null) {
      this.relationWhere = locationSet.getRelationship();
    }
    setListCriteria(this.relationWhere + sqlWhere);
    return super.getList();
  }
}
