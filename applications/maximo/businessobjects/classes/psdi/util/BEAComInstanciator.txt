/*
 *
 * IBM Confidential
 *
 * OCO Source Materials
 *
 * 5724-R46
 *
 * (C) COPYRIGHT IBM CORP. 2006,2007
 *
 * The source code for this program is not published or otherwise
 * divested of its trade secrets, irrespective of what has been
 * deposited with the U.S. Copyright Office.
 *
 */
package psdi.util;
import psdi.util.logging.MXLogger;
import psdi.util.logging.FixedLoggers;
import com.linar.jintegra.*;

public class BEAComInstanciator implements com.bea.jcom.Instanciator
{
	private static MXLogger logger = FixedLoggers.MAXIMOLOGGER;
	public Object instanciate(String javaClass) throws AutomationException
	{
		logger.debug("the req class is ===="+javaClass);
		try
		{
			return Class.forName(javaClass).newInstance();
		}
		catch (Exception t)
		{
			logger.error("Error loading class",t);
			return null;
		}
	}
}
