CREATE VIEW CISADM.ci_mr_all AS 
SELECT mr_id
,mtr_config_id
,read_dttm
,use_on_bill_sw
,version
,mr_source_cd
,mtr_reader_id
FROM CISADM.ci_mr@CSXP.WORLD;

CREATE VIEW CISADM.ci_mtr_all AS 
SELECT mtr_id
,badge_nbr
,mtr_type_cd
,mtr_status_flg
,mfg_cd
,model_cd
,serial_nbr
,receive_dt
,retire_dt
,descr254
,version
,retire_rsn_cd
FROM CISADM.ci_mtr@CSXP.WORLD;

CREATE VIEW CISADM.ci_mtr_config_all AS 
SELECT mtr_config_id
,mtr_id
,eff_dttm
,version
,mtr_config_ty_cd
FROM CISADM.ci_mtr_config@CSXP.WORLD;

CREATE VIEW CISADM.ci_prem_all AS 
SELECT prem_id
,prem_type_cd
,cis_division
,ll_id
,key_sw
,key_id
,ok_to_enter_sw
,mr_instr_cd
,mr_instr_details
,mr_warn_cd
,trend_area_cd
,address1
,address2
,address3
,address4
,mail_addr_sw
,city
,num1
,num2
,county
,postal
,house_type
,geo_code
,in_city_limit
,state
,country
,version
,address1_upr
,city_upr
,time_zone_cd
,ls_sl_flg
,ls_sl_descr
,prnt_prem_id
FROM CISADM.ci_prem@CSXP.WORLD;

CREATE VIEW CISADM.ci_prem_char_all AS 
SELECT prem_id
,char_type_cd
,char_val
,effdt
,version
,adhoc_char_val
,char_val_fk1
,char_val_fk2
,char_val_fk3
,char_val_fk4
,char_val_fk5 
FROM CISADM.ci_prem_char@CSXP.WORLD;

CREATE VIEW CISADM.ci_sp_all AS 
SELECT discon_loc_cd
,sp_id
,sp_type_cd
,prem_id
,sp_status_flg
,install_dt
,sp_src_status_flg
,mr_cyc_cd
,mr_rte_cd
,mtr_loc_cd
,mr_cyc_rte_seq
,fac_lvl_1_cd
,mtr_loc_details
,fac_lvl_2_cd
,fac_lvl_3_cd
,version
,descr254
,abolish_dt
,ovrd_prem_fld_info_flg
,mr_instr_cd
,mr_warn_cd
,key_sw
,key_id
,ok_to_enter_sw
,mr_instr_details
FROM CISADM.ci_sp@CSXP.WORLD;

CREATE VIEW CISADM.ci_sp_char_all AS 
SELECT sp_id
,char_type_cd
,char_val
,effdt
,version
,adhoc_char_val
,char_val_fk1
,char_val_fk2
,char_val_fk3
,char_val_fk4
,char_val_fk5 
FROM CISADM.ci_sp_char@CSXP.WORLD;

CREATE VIEW CISADM.ci_sp_geo_all AS 
SELECT sp_id
,geo_type_cd
,geo_val
,version 
FROM CISADM.ci_sp_geo@CSXP.WORLD;

CREATE VIEW CISADM.ci_sp_mtr_evt_all AS 
SELECT sp_mtr_hist_id
,seqno
,mr_id
,sp_mtr_evt_flg
,mtr_on_off_flg
,version 
FROM CISADM.ci_sp_mtr_evt@CSXP.WORLD;

CREATE VIEW CISADM.ci_sp_mtr_hist_all AS 
SELECT sp_mtr_hist_id
,mtr_config_id
,sp_id
,removal_mr_id
,removal_dttm
,version
,install_const 
FROM CISADM.ci_sp_mtr_hist@CSXP.WORLD;

GRANT SELECT ON CISADM.ci_lookup TO DATAREC;
GRANT SELECT ON CISADM.ci_mr_all TO DATAREC;
GRANT SELECT ON CISADM.ci_mtr_all TO DATAREC;
GRANT SELECT ON CISADM.ci_mtr_config_all TO DATAREC;
GRANT SELECT ON CISADM.ci_prem_all TO DATAREC;
GRANT SELECT ON CISADM.ci_prem_char_all TO DATAREC;
GRANT SELECT ON CISADM.ci_sp_all TO DATAREC;
GRANT SELECT ON CISADM.ci_sp_char_all TO DATAREC;
GRANT SELECT ON CISADM.ci_sp_geo_all TO DATAREC;
GRANT SELECT ON CISADM.ci_sp_mtr_evt_all TO DATAREC;
GRANT SELECT ON CISADM.ci_sp_mtr_hist_all TO DATAREC;
