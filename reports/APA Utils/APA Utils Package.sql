CREATE OR REPLACE PACKAGE apa_utils AS
  FUNCTION fn_getlocaltime
           (
             mydate DATE,
             state VARCHAR2
           )
    RETURN DATE;
END apa_utils;
/

CREATE OR REPLACE PACKAGE BODY apa_utils AS
  
  FUNCTION fn_getlocaltime
           (
             mydate DATE,
             state VARCHAR2
           )
    RETURN DATE IS

    TYPE ty_refcur IS REF CURSOR;
    TYPE ty_timeshiftrectype IS RECORD
    (
      localtime TIMESTAMP
    );
    lv_timeshift_cur ty_refcur;
    lv_timeshiftrec  ty_timeshiftrectype;
    lv_timeshift_sql VARCHAR2(200);
    lv_state         VARCHAR2(20);
    lv_timezone      VARCHAR2(20);
    
  BEGIN
    lv_state := UPPER(state);
    lv_timezone := CASE
                    WHEN lv_state IN ('ACT','AUSTRALIAN CAPITAL TERRITORY') THEN 'Australia/ACT'
                    WHEN lv_state IN ('NSW','NEW SOUTH WALES') THEN 'Australia/NSW'
                    WHEN lv_state IN ('NT','NORTHERN TERRITORY') THEN 'Australia/North'
                    WHEN lv_state IN ('SA','SOUTH AUSTRALIA') THEN 'Australia/South'
                    WHEN lv_state IN ('TAS','TASMANIA') THEN 'Australia/Tasmania'
                    WHEN lv_state IN ('WA','WESTERN AUSTRALIA') THEN 'Australia/West'
                    WHEN lv_state IN ('VIC','VICTORIA') THEN 'Australia/Victoria'
                    ELSE 'Australia/Queensland'
                  END;
    
    lv_timeshift_sql := 'SELECT FROM_TZ(CAST(TO_DATE(''' || TO_CHAR(mydate,'dd/mm/yyyy hh24:mi:ss') || ''',''dd/mm/yyyy hh24:mi:ss'') AS TIMESTAMP),''Australia/Queensland'') AT TIME ZONE ''' || lv_timezone || ''' AS localtime FROM DUAL';
    OPEN lv_timeshift_cur FOR lv_timeshift_sql;
    FETCH lv_timeshift_cur INTO lv_timeshiftrec;
    CLOSE lv_timeshift_cur;
    
    RETURN lv_timeshiftrec.localtime;
    
  END fn_getlocaltime;
END apa_utils;
/

SHOW ERRORS
/

GRANT EXECUTE ON apa_utils TO readonly, datarec
/
