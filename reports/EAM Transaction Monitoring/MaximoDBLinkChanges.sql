--
-- To be run as a DBA
--

-- Create readonly public link to National Reporting database
-- Service name of NR DB to be changed according to environment being deployed to
CREATE PUBLIC DATABASE LINK rdb_link
CONNECT TO readonly IDENTIFIED BY &RDPREADONLYPASSWORD
USING '&RDPDB'
/
