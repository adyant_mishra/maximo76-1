CREATE TABLE tm.gasmeternotification (
  gdndivision                   VARCHAR2(4) NOT NULL,
  gmndocid                      VARCHAR2(64),
  gmnmessageid                  VARCHAR2(64),
  gmntxnmessagedate             DATE,
  gmntxnpriority                VARCHAR2(64),
  gmntxnrecipient               VARCHAR2(64),
  gmntxnsender                  VARCHAR2(64),
  gmntransactionid              VARCHAR2(64),
  gmntransactiondate            DATE,
  gmnmirn                       VARCHAR2(64),
  gmnmirnstatus                 VARCHAR2(64),
  gmnordercompleteddate         DATE,
  rctreceipttype                VARCHAR2(64),
  rctdocid                      VARCHAR2(64),
  rctmessageid                  VARCHAR2(64),
  rctreceiptid                  VARCHAR2(64),
  rctreceiptdate                DATE,
  rcteventclass                 VARCHAR2(64),
  rcteventcode                  VARCHAR2(4),
  rcteventcodedesc              VARCHAR2(512),
  rcteventexplanation           VARCHAR2(512),
  rcteventkeyinfo               VARCHAR2(64),
  rcteventseverity              VARCHAR2(64),
  serviceorderno                VARCHAR2(64)
)
/

CREATE INDEX tm.gasmeternotification_idx1 
ON tm.gasmeternotification (gdndivision, gmndocid, gmnmessageid)
/

CREATE INDEX tm.gasmeternotification_idx2
ON tm.gasmeternotification (gdndivision, gmntransactionid)
/

CREATE INDEX tm.gasmeternotification_idx3
ON tm.gasmeternotification (gmnmirn)
/

CREATE INDEX tm.gasmeternotification_idx4
ON tm.gasmeternotification (gdndivision, serviceorderno)
/

CREATE INDEX tm.gasmeternotification_idx5
ON tm.gasmeternotification (rctreceiptdate)
/

GRANT SELECT ON tm.gasmeternotification TO readonly
/
