SET SERVEROUT ON

ALTER TABLE tm.txnlifecycle ADD (
  wmmirn                        VARCHAR2(64),
  wmrecipient                   VARCHAR2(64),
  wmsender                      VARCHAR2(64),
  wmrejecteddate                DATE,
  cdxwfstatus                   VARCHAR2(60),
  cdxfastatus                   VARCHAR2(60),
  cdxfastagestatus              VARCHAR2(2),
  cdxgmntoaemodate              DATE
)
/

DECLARE
  CURSOR cr_tlc IS
    SELECT *
    FROM   tm.txnlifecycle tlc
    FOR UPDATE;
    tlc_count INTEGER := 0;
    
  CURSOR cr_rej (ci_gdndivision    VARCHAR2,
                 ci_wmoriginatingtransactionid VARCHAR2)
  IS
    SELECT MAX(receiptdate) AS receiptdate
    FROM   asetxnackreject anr
    WHERE  anr.gdndivision = ci_gdndivision
    AND    anr.initiatingtransactionid = ci_wmoriginatingtransactionid;
  rej_rec cr_rej%ROWTYPE;
  rej_count INTEGER := 0;

  CURSOR cr_sri (ci_gdndivision    VARCHAR2,
                 ci_wmserviceorder VARCHAR2)
  IS
    SELECT mirn
    FROM   sordresponseinitial sri
    WHERE  sri.gdndivision = ci_gdndivision
    AND    sri.serviceorderno = ci_wmserviceorder;
  sri_rec cr_sri%ROWTYPE;
  sri_count INTEGER := 0;

  CURSOR cr_srn (ci_gdndivision    VARCHAR2,
                 ci_wmserviceorder VARCHAR2)
  IS
    SELECT atx.recipient,
           atx.sender
    FROM   sordrequestnew srn
    JOIN   asetxn atx
       ON  atx.gdndivision = srn.gdndivision
       AND atx.docid = srn.docid
       AND atx.messageid = srn.messageid
    WHERE  srn.gdndivision = ci_gdndivision
    AND    srn.serviceorderno = ci_wmserviceorder;
  srn_rec cr_srn%ROWTYPE;
  srn_count INTEGER := 0;

  CURSOR cr_cdx_wf (ci_cdxwfprocid VARCHAR2)
  IS
    SELECT lok.descr
    FROM   cisadm.ci_wf_proc@cdx_link wf
    JOIN   cisadm.ci_lookup@cdx_link lok
       ON  lok.field_name = 'WF_STAT_RSN_FLG'
       AND wf.wf_stat_rsn_flg = lok.field_value
    WHERE  wf.wf_proc_id = ci_cdxwfprocid;
  cdx_wf_rec cr_cdx_wf%ROWTYPE;
  cdx_wf_count INTEGER := 0;

  CURSOR cr_cdx_fa (ci_cdxfaid VARCHAR2)
  IS
    SELECT lok.descr,
           fsu.fa_up_status_flg
    FROM   cisadm.ci_fa@cdx_link fa
    JOIN   cisadm.ci_lookup@cdx_link lok
       ON  lok.field_name = 'FA_STATUS_FLG'
       AND fa.fa_status_flg = lok.field_value
    LEFT OUTER JOIN cisadm.ci_fa_stage_up@cdx_link fsu
       ON  fsu.fa_id = fa.fa_id
    WHERE  fa.fa_id = ci_cdxfaid;
  cdx_fa_rec cr_cdx_fa%ROWTYPE;
  cdx_fa_count INTEGER := 0;

  CURSOR cr_cdx_wf_gmn (ci_cdxwfprocid VARCHAR2)
  IS
    SELECT MAX(INSTR(TO_CHAR(xai.xml_request),'<MessageDate')) AS aemo_date_pos,          
           MAX(REPLACE(SUBSTR(TO_CHAR(xai.xml_request),INSTR(TO_CHAR(xai.xml_request),'<MessageDate')+13,19),'T',' ')) AS aemo_date
    FROM   cisadm.ci_wf_evt_ctxt@cdx_link wctx
    JOIN   cisadm.ci_xai_dwn@cdx_link xai
       ON  xai.nt_dwn_id = wctx.ctxt_val
       AND xai.xai_rt_type_cd ='VIGASMTRNO'
    WHERE  wctx.wf_proc_id = ci_cdxwfprocid
    AND    wctx.wf_evt_ctxt_flg = 'NDS';
  cdx_wf_gmn_rec cr_cdx_wf_gmn%ROWTYPE;
  cdx_wf_gmn_count INTEGER := 0;

BEGIN
  FOR tlc_rec IN cr_tlc LOOP
    tlc_count := tlc_count + 1;

    rej_rec := NULL;
    sri_rec := NULL;
    srn_rec := NULL;
    cdx_wf_rec := NULL;
    cdx_fa_rec := NULL;
    cdx_wf_gmn_rec := NULL;
    IF tlc_rec.wmserviceorder IS NOT NULL THEN
      OPEN cr_rej (ci_gdndivision    => tlc_rec.gdndivision,
                   ci_wmoriginatingtransactionid => tlc_rec.wmoriginatingtransactionid);
      FETCH cr_rej INTO rej_rec;
      IF cr_rej%FOUND THEN 
        rej_count := rej_count + 1;
      END IF;
      CLOSE cr_rej;

      OPEN cr_sri (ci_gdndivision    => tlc_rec.gdndivision,
                   ci_wmserviceorder => tlc_rec.wmserviceorder);
      FETCH cr_sri INTO sri_rec;
      IF cr_sri%FOUND THEN 
        sri_count := sri_count + 1;
      END IF;
      CLOSE cr_sri;

      OPEN cr_srn (ci_gdndivision    => tlc_rec.gdndivision,
                   ci_wmserviceorder => tlc_rec.wmserviceorder);
      FETCH cr_srn INTO srn_rec;
      IF cr_srn%FOUND THEN 
        srn_count := srn_count + 1;
      END IF;
      CLOSE cr_srn;
    END IF;
  
    IF tlc_rec.cdxwfprocid IS NOT NULL THEN
      OPEN cr_cdx_wf (ci_cdxwfprocid => tlc_rec.cdxwfprocid);
      FETCH cr_cdx_wf INTO cdx_wf_rec;
      IF cr_cdx_wf%FOUND THEN 
        cdx_wf_count := cdx_wf_count + 1;
      END IF;
      CLOSE cr_cdx_wf;
    END IF;
    
    IF tlc_rec.cdxfaid IS NOT NULL THEN
      OPEN cr_cdx_fa (ci_cdxfaid => tlc_rec.cdxfaid);
      FETCH cr_cdx_fa INTO cdx_fa_rec;
      IF cr_cdx_fa%FOUND THEN 
        cdx_fa_count := cdx_fa_count + 1;
      END IF;
      CLOSE cr_cdx_fa;
    END IF;
    
    IF tlc_rec.cdxwfprocid IS NOT NULL AND tlc_rec.wmjobenquirycode = 'MFX' THEN
      OPEN cr_cdx_wf_gmn (ci_cdxwfprocid => tlc_rec.cdxwfprocid);
      FETCH cr_cdx_wf_gmn INTO cdx_wf_gmn_rec;
      IF cdx_wf_gmn_rec.aemo_date_pos > 0 THEN 
        cdx_wf_gmn_count := cdx_wf_gmn_count + 1;
      ELSE
        cdx_wf_gmn_rec.aemo_date := NULL;
      END IF;
      CLOSE cr_cdx_wf_gmn;
    END IF;

    UPDATE tm.txnlifecycle
    SET    wmmirn           = sri_rec.mirn,
           wmrecipient      = srn_rec.recipient,
           wmsender         = srn_rec.sender,
           wmrejecteddate   = rej_rec.receiptdate,
           cdxwfstatus      = cdx_wf_rec.descr,
           cdxfastatus      = cdx_fa_rec.descr,
           cdxfastagestatus = cdx_fa_rec.fa_up_status_flg,
           cdxgmntoaemodate = TO_DATE(cdx_wf_gmn_rec.aemo_date,'YYYY-MM-DD HH24:MI:SS')
    WHERE CURRENT OF cr_tlc;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('tlc_count:' || tlc_count);
  DBMS_OUTPUT.PUT_LINE('rej_count:' || rej_count);
  DBMS_OUTPUT.PUT_LINE('sri_count:' || sri_count);
  DBMS_OUTPUT.PUT_LINE('srn_count:' || srn_count);
  DBMS_OUTPUT.PUT_LINE('cdx_wf_count:' || cdx_wf_count);
  DBMS_OUTPUT.PUT_LINE('cdx_fa_count:' || cdx_fa_count);
  DBMS_OUTPUT.PUT_LINE('cdx_wf_gmn_count:' || cdx_wf_gmn_count);
END;
/

ALTER TABLE tm.txnlifecycle DROP (
  wmmirn,
  wmrecipient,
  wmsender,
  wmrejecteddate,
  cdxwfstatus,
  cdxfastatus,
  cdxfastagestatus,
  cdxgmntoaemodate
)
/

