CREATE OR REPLACE PACKAGE tm.populate_txnlifecycle AS

  PROCEDURE main;

END populate_txnlifecycle;
/

SHOW ERRORS
/

CREATE OR REPLACE PACKAGE BODY tm.populate_txnlifecycle AS  

  pkv_txnpopid INTEGER;
  pkv_sord_cutoff_date DATE;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Write history record
  ------------------------------------------------------------------------------------------------------------------
  FUNCTION fn_create_run_history
    RETURN INTEGER IS
    CURSOR cr_get_lastrun_latestsorddate IS
      SELECT latestsorddate
      FROM   txnpopulationhistory
      WHERE  runstatus = 'COMPLETE'
      AND    txnpopid = (SELECT MAX(txnpopid)
                         FROM   txnpopulationhistory);
  BEGIN
    -- Set up cutoff date for query of service orders using date populated in last complete run
    -- Subtract one day to allow for any misalignment of dates 
    OPEN cr_get_lastrun_latestsorddate;
    FETCH cr_get_lastrun_latestsorddate INTO pkv_sord_cutoff_date;
    CLOSE cr_get_lastrun_latestsorddate;
    IF pkv_sord_cutoff_date IS NULL THEN
      pkv_sord_cutoff_date := SYSDATE - 1;
    END IF;  
    INSERT INTO txnpopulationhistory (
      txnpopid,
      startdate,
      runstatus
    ) VALUES (
      txnpopulationseq.NEXTVAL,
      SYSDATE,
      'STARTED'
    );
    COMMIT;
    RETURN txnpopulationseq.CURRVAL;    
  END fn_create_run_history;
  
  ------------------------------------------------------------------------------------------------------------------
  -- Create history step to record timings
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_create_run_history_step(
    prv_step               INTEGER,
    prv_step_description   VARCHAR2
  ) IS
  BEGIN
    INSERT INTO txnpopulationstephistory (
      txnpopid,
      stepid,
      stepdescription,
      startdate
    ) VALUES (
      pkv_txnpopid,
      prv_step,
      prv_step_description,
      SYSDATE
    );
    COMMIT;
  END pr_create_run_history_step;

  ------------------------------------------------------------------------------------------------------------------
  -- Create history step to record timings
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_update_run_history_step(
    prv_step         VARCHAR2,
    prv_record_count INTEGER DEFAULT NULL
  ) IS
  BEGIN
    UPDATE txnpopulationstephistory
    SET    enddate = SYSDATE,
           recordcount = prv_record_count
    WHERE  txnpopid = pkv_txnpopid
    AND    stepid = prv_step;
    COMMIT;
  END pr_update_run_history_step;

  ------------------------------------------------------------------------------------------------------------------
  -- Update status in ccbdatarec history record
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_update_run_history(
    prv_status  VARCHAR2,
    prv_enddate DATE
  ) IS
    CURSOR cr_get_latest_sorddate IS
      SELECT MAX(wmoriginatingtransactiondate) AS maxsorddate
      FROM   txnlifecycle;
  BEGIN
    OPEN cr_get_latest_sorddate;
    FETCH cr_get_latest_sorddate INTO pkv_sord_cutoff_date;
    CLOSE cr_get_latest_sorddate;
    UPDATE txnpopulationhistory
    SET    runstatus = prv_status,
           enddate = prv_enddate,
           latestsorddate = pkv_sord_cutoff_date
    WHERE  txnpopid = pkv_txnpopid;
    COMMIT;
  END pr_update_run_history;

  ------------------------------------------------------------------------------------------------------------------
  -- Add new retailer service order requests to lifecycle table
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_insert_new_sords IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '10',
      prv_step_description  => 'Insert new retailer service orders');
     
    -- Insert new records where GDN and service order has not been encountered previously
    INSERT INTO txnlifecycle (
           transactionprocessingcomplete,
           gdndivision,
           wmserviceorder,
           wmjobenquirycode,
           wmoriginatingtransactionid,
           wmoriginatingtransactiondate,
           wmpreferredappointmentdate,
           wmrecipient,
           wmsender
    )
    SELECT 'N',
           srn.gdndivision,
           srn.serviceorderno,
           srn.jobenquirycode,
           srn.transactionid,
           srn.transactiondate,
           srn.appointmentpreferreddate,
           atx.recipient,
           atx.sender
    FROM   sordrequestnew srn
    JOIN   asetxn atx
       ON  atx.gdndivision = srn.gdndivision
       AND atx.docid = srn.docid
       AND atx.messageid = srn.messageid
    WHERE  srn.transactiondate > pkv_sord_cutoff_date - 1
    AND NOT EXISTS
           (
              SELECT 'x'
              FROM   txnlifecycle tlc
              WHERE  tlc.gdndivision = srn.gdndivision
              AND    tlc.wmserviceorder = srn.serviceorderno
           )
    AND NOT EXISTS
           (
              SELECT 'x'
              FROM   sordrequestnew srn2
              WHERE  srn2.gdndivision = srn.gdndivision
              AND    srn2.serviceorderno = srn.serviceorderno
              AND    srn2.docid > srn.docid
           );
    lv_record_count := SQL%ROWCOUNT;
    pr_update_run_history_step(
      prv_step => '10',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_insert_new_sords;

  ------------------------------------------------------------------------------------------------------------------
  -- Add new Gas Meter Notification records that have been created
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_insert_new_gmns IS
    lv_record_count INTEGER := 0;

    CURSOR cr_gmns IS
      SELECT *
      FROM   sordgasmeternotification sordgmn
      WHERE  sordgmn.transactiondate > pkv_sord_cutoff_date - 1
      AND NOT EXISTS
             (
                SELECT 'x'
                FROM   gasmeternotification gmn
                WHERE  gmn.gdndivision = sordgmn.gdndivision
                AND    gmn.gmntransactionid = sordgmn.transactionid
             );

    CURSOR cr_asetxn(ci_gdndivision VARCHAR2,
                     ci_docid       VARCHAR2,
                     ci_messageid   VARCHAR2)
    IS
      SELECT *
      FROM   asetxn
      WHERE  gdndivision = ci_gdndivision
      AND    docid = ci_docid
      AND    messageid = ci_messageid;

    CURSOR cr_receipt(ci_gdndivision   VARCHAR2,
                      ci_transactionid VARCHAR2)
    IS
      SELECT docid,
             messageid,
             initiatingtransactionid,
             gdndivision,
             'Accept' AS receipttype,
             receiptid,
             receiptdate,
             NULL AS eventclass,
             NULL AS eventcode,
             NULL AS eventcodedesc,
             NULL AS eventcontext,
             NULL AS eventexplanation,
             NULL AS eventkeyinfo,
             NULL AS eventseverity
      FROM   asetxnackaccept
      WHERE  gdndivision = ci_gdndivision
      AND    initiatingtransactionid = ci_transactionid
      UNION ALL
      SELECT docid,
             messageid,
             initiatingtransactionid,
             gdndivision,
             'Partial' AS receipttype,
             receiptid,
             receiptdate,
             eventclass,
             eventcode,
             eventcodedesc,
             eventcontext,
             eventexplanation,
             eventkeyinfo,
             eventseverity
      FROM   asetxnackpartial
      WHERE  gdndivision = ci_gdndivision
      AND    initiatingtransactionid = ci_transactionid
      UNION ALL
      SELECT docid,
             messageid,
             initiatingtransactionid,
             gdndivision,
             'Reject' AS receipttype,
             receiptid,
             receiptdate,
             eventclass,
             eventcode,
             eventcodedesc,
             eventcontext,
             eventexplanation,
             eventkeyinfo,
             eventseverity
      FROM   asetxnackreject
      WHERE  gdndivision = ci_gdndivision
      AND    initiatingtransactionid = ci_transactionid;

    CURSOR cr_ccbdwn(ci_transactionid VARCHAR2)
    IS
      SELECT (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = dwn.nt_dwn_id
               AND    ctxt.nt_dwn_ctxt_flg = 'NMI'
             ) AS mirn,  
             (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = dwn.nt_dwn_id
               AND    ctxt.nt_dwn_ctxt_flg = 'MSTS'
             ) AS mirnstatus,
             (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = dwn.nt_dwn_id
               AND    ctxt.nt_dwn_ctxt_flg = 'SONB'
             ) AS serviceorderno
      FROM   cisadm.ci_nt_dwn@cdx_link dwn
      WHERE  nt_dwn_id = ci_transactionid;

    lv_asetxn_rec asetxn%ROWTYPE;
    lv_gasmeternotification_rec gasmeternotification%ROWTYPE;
    lv_receipt_rec cr_receipt%ROWTYPE;
    lv_ccbdwn_rec cr_ccbdwn%ROWTYPE;
    
  BEGIN
    pr_create_run_history_step(
      prv_step              => '13',
      prv_step_description  => 'Insert new Gas Meter Notifications');

    FOR lv_gmn_rec IN cr_gmns LOOP
      -- Get ASETXN record for GMN
      OPEN cr_asetxn(ci_gdndivision => lv_gmn_rec.gdndivision,
                     ci_docid       => lv_gmn_rec.docid,
                     ci_messageid   => lv_gmn_rec.messageid);
      FETCH cr_asetxn INTO lv_asetxn_rec;
      CLOSE cr_asetxn;
      lv_gasmeternotification_rec.gdndivision := lv_gmn_rec.gdndivision;
      lv_gasmeternotification_rec.gmndocid := lv_gmn_rec.docid;
      lv_gasmeternotification_rec.gmnmessageid := lv_gmn_rec.messageid;
      lv_gasmeternotification_rec.gmntxnmessagedate := lv_asetxn_rec.messagedate;
      lv_gasmeternotification_rec.gmntxnpriority := lv_asetxn_rec.priority;
      lv_gasmeternotification_rec.gmntxnrecipient := lv_asetxn_rec.recipient;
      lv_gasmeternotification_rec.gmntxnsender := lv_asetxn_rec.sender;
      lv_gasmeternotification_rec.gmntransactionid := lv_gmn_rec.transactionid;
      lv_gasmeternotification_rec.gmntransactiondate := lv_gmn_rec.transactiondate;
      lv_gasmeternotification_rec.gmnordercompleteddate  := lv_gmn_rec.ordercompleteddate;

      -- Get receipt for GMN
      OPEN cr_receipt(ci_gdndivision   => lv_gmn_rec.gdndivision,
                      ci_transactionid => lv_gmn_rec.transactionid);
      FETCH cr_receipt INTO lv_receipt_rec;
      CLOSE cr_receipt;
      lv_gasmeternotification_rec.rctreceipttype := lv_receipt_rec.receipttype;
      lv_gasmeternotification_rec.rctdocid := lv_receipt_rec.docid;
      lv_gasmeternotification_rec.rctmessageid := lv_receipt_rec.messageid;
      lv_gasmeternotification_rec.rctreceiptid := lv_receipt_rec.receiptid;
      lv_gasmeternotification_rec.rctreceiptdate := lv_receipt_rec.receiptdate;
      lv_gasmeternotification_rec.rcteventclass := lv_receipt_rec.eventclass;
      lv_gasmeternotification_rec.rcteventcode := lv_receipt_rec.eventcode;
      lv_gasmeternotification_rec.rcteventcodedesc := lv_receipt_rec.eventcodedesc;
      lv_gasmeternotification_rec.rcteventexplanation := lv_receipt_rec.eventexplanation;
      lv_gasmeternotification_rec.rcteventkeyinfo := lv_receipt_rec.eventkeyinfo;
      lv_gasmeternotification_rec.rcteventseverity := lv_receipt_rec.eventseverity;

      -- Get CCB details for GMN
      lv_ccbdwn_rec := NULL;
      OPEN cr_ccbdwn(ci_transactionid => SUBSTR(lv_gmn_rec.transactionid,INSTR(lv_gmn_rec.transactionid,'Txn')+4));
      FETCH cr_ccbdwn INTO lv_ccbdwn_rec;
      CLOSE cr_ccbdwn;
     
      lv_gasmeternotification_rec.gmnmirn := COALESCE(lv_gmn_rec.mirn,lv_ccbdwn_rec.mirn);
      lv_gasmeternotification_rec.gmnmirnstatus := COALESCE(lv_gmn_rec.mirnstatus,lv_ccbdwn_rec.mirnstatus);
      lv_gasmeternotification_rec.serviceorderno := lv_ccbdwn_rec.serviceorderno;

      INSERT INTO gasmeternotification(
        gdndivision,
        gmndocid,
        gmnmessageid,
        gmntxnmessagedate,
        gmntxnpriority,
        gmntxnrecipient,
        gmntxnsender,
        gmntransactionid,
        gmntransactiondate,
        gmnmirn,
        gmnmirnstatus,
        gmnordercompleteddate,
        rctreceipttype,
        rctdocid,
        rctmessageid,
        rctreceiptid,
        rctreceiptdate,
        rcteventclass,
        rcteventcode,
        rcteventcodedesc,
        rcteventexplanation,
        rcteventkeyinfo,
        rcteventseverity,
        serviceorderno
      ) VALUES (
        lv_gasmeternotification_rec.gdndivision,
        lv_gasmeternotification_rec.gmndocid,
        lv_gasmeternotification_rec.gmnmessageid,
        lv_gasmeternotification_rec.gmntxnmessagedate,
        lv_gasmeternotification_rec.gmntxnpriority,
        lv_gasmeternotification_rec.gmntxnrecipient,
        lv_gasmeternotification_rec.gmntxnsender,
        lv_gasmeternotification_rec.gmntransactionid,
        lv_gasmeternotification_rec.gmntransactiondate,
        lv_gasmeternotification_rec.gmnmirn,
        lv_gasmeternotification_rec.gmnmirnstatus,
        lv_gasmeternotification_rec.gmnordercompleteddate,
        lv_gasmeternotification_rec.rctreceipttype,
        lv_gasmeternotification_rec.rctdocid,
        lv_gasmeternotification_rec.rctmessageid,
        lv_gasmeternotification_rec.rctreceiptid,
        lv_gasmeternotification_rec.rctreceiptdate,
        lv_gasmeternotification_rec.rcteventclass,
        lv_gasmeternotification_rec.rcteventcode,
        lv_gasmeternotification_rec.rcteventcodedesc,
        lv_gasmeternotification_rec.rcteventexplanation,
        lv_gasmeternotification_rec.rcteventkeyinfo,
        lv_gasmeternotification_rec.rcteventseverity,
        lv_gasmeternotification_rec.serviceorderno
      );
    lv_record_count := lv_record_count + 1;
    END LOOP;

    pr_update_run_history_step(
      prv_step => '13',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_insert_new_gmns;

---------------------------------------------------------------------------------------------------------------------------
-- Update gasmeternotification where status has changed from error to accept
---------------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_gmn_update IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '14',
      prv_step_description  => 'Update rejected gas meter notification');

      UPDATE gasmeternotification gmn
      SET (rctreceipttype,
              rctdocid,
              rctmessageid,
              rctreceiptid,
              rctreceiptdate, 
              rcteventclass,
              rcteventcode,
              rcteventcodedesc,
              rcteventexplanation,
              rcteventkeyinfo,
              rcteventseverity) = (
                                   SELECT 
                                   'Accept' AS receipttype,
                                   docid,
                                   messageid,
                                   receiptid,
                                   receiptdate,
                                   NULL AS rcteventclass,
                                   NULL AS rcteventcode,
                                   NULL AS rcteventcodedesc,
                                   NULL AS eventexplanation,
                                   NULL AS eventkeyinfo,
                                   NULL AS eventseverity
                                   FROM asetxnackaccept acp
                                   WHERE acp.gdndivision = gmn.gdndivision
                                   AND acp.INITIATINGTRANSACTIONID =  gmn.gmntransactionid
                                   )
      WHERE rctreceipttype IN ('Reject','Partial')
      AND   gmntxnrecipient IN ('VENCORP','REMCO','NAGMO')
      AND EXISTS
            (
              SELECT 1
              FROM asetxnackaccept acp
              WHERE acp.gdndivision = gmn.gdndivision
              AND   acp.initiatingtransactionid =  gmn.gmntransactionid
            );
    pr_update_run_history_step(
      prv_step => '14',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_gmn_update; 
  
  ------------------------------------------------------------------------------------------------------------------
  -- Update rejected service orders with date of rejection and set record to prevent further processing
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_sord_rejected IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '15',
      prv_step_description  => 'Service order rejected');
     
    UPDATE txnlifecycle tlc
    SET    wmrejecteddate =
           (
             SELECT MAX(receiptdate)
             FROM   asetxnackreject anr
             WHERE  anr.gdndivision = tlc.gdndivision
             AND    (anr.initiatingtransactionid = tlc.wmoriginatingtransactionid
                 OR   (tlc.wmoriginatingtransactionid IS NULL
                   AND anr.initiatingtransactionid = tlc.wmcancellationtransactionid))
           )
    WHERE  tlc.transactionprocessingcomplete = 'N';
    lv_record_count := SQL%ROWCOUNT;
    UPDATE txnlifecycle tlc
    SET    tlc.transactionprocessingcomplete = 'Y'    
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND     wmrejecteddate IS NOT NULL;
    pr_update_run_history_step(
      prv_step => '15',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_sord_rejected;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with date that initial response was sent to retailer and MIRN associated with address
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_sord_initial_response IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '20',
      prv_step_description  => 'Initial response to new service order');
     
    UPDATE txnlifecycle tlc
    SET    (wminitialresponsedate, wmmirn) =
           (
             SELECT MAX(transactiondate), MAX(mirn)
             FROM   sordresponseinitial sri
             WHERE  sri.gdndivision = tlc.gdndivision
             AND    sri.serviceorderno = tlc.wmserviceorder
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.wmserviceorder IS NOT NULL;
    lv_record_count := SQL%ROWCOUNT;
    pr_update_run_history_step(
      prv_step => '20',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_sord_initial_response;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with details of CCB workflow and field activity creation
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_cdx_new_wf_fa IS
    lv_record_count INTEGER := 0;
    
    CURSOR cr_incomplete_txns IS
      SELECT *
      FROM   txnlifecycle tlc
      WHERE  tlc.transactionprocessingcomplete = 'N'
      AND    tlc.wmserviceorder IS NOT NULL
      AND    (tlc.cdxwfprocid IS NULL
           OR tlc.cdxfaid IS NULL)
      FOR UPDATE OF cdxwfprocid;

    CURSOR cr_cdx_details(cv_service_order  VARCHAR2,
                          cv_transaction_id VARCHAR2) IS
      SELECT wf.wf_proc_id,
             wf.cre_dttm AS wf_creation_date,
             fa.fa_id,
             fa.cre_dttm AS fa_creation_date
      FROM   cisadm.ci_wf_proc_ctxt@cdx_link wfsonb
      JOIN   cisadm.ci_wf_proc@cdx_link wf
         ON  wfsonb.wf_proc_id = wf.wf_proc_id
      JOIN   cisadm.ci_lookup@cdx_link lok
         ON  lok.field_name = 'WF_STAT_RSN_FLG'
         AND wf.wf_stat_rsn_flg = lok.field_value
      JOIN   cisadm.ci_wf_proc_ctxt@cdx_link wfatty
         ON  wfatty.wf_proc_id = wf.wf_proc_id
         AND wfatty.wf_proc_ctxt_flg = 'ATTY'
         AND COALESCE(wfatty.ctxt_val,wfatty.ctxt_val) = 'New'
      JOIN   cisadm.ci_wf_proc_ctxt@cdx_link wftrid
         ON  wftrid.wf_proc_id = wf.wf_proc_id
         AND wftrid.wf_proc_ctxt_flg = 'TRID'
         AND wftrid.ctxt_val = RPAD(cv_transaction_id,50)
      LEFT OUTER JOIN cisadm.ci_wf_proc_ctxt@cdx_link facty
         ON  facty.wf_proc_id = wf.wf_proc_id
         AND facty.wf_proc_ctxt_flg = 'SPRF'
      LEFT OUTER JOIN cisadm.ci_fa@cdx_link fa
         ON  fa.fa_id = facty.ctxt_val
      WHERE wfsonb.wf_proc_ctxt_flg = 'SONB'
      AND   wfsonb.ctxt_val = RPAD(cv_service_order,50);
    lv_cdx_rec cr_cdx_details%ROWTYPE;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '30',
      prv_step_description  => 'CCB workflow and field activity for service order');
    
    FOR lv_txn_rec IN cr_incomplete_txns LOOP
      OPEN cr_cdx_details(cv_service_order  => lv_txn_rec.wmserviceorder,
                          cv_transaction_id => lv_txn_rec.wmoriginatingtransactionid);
      FETCH cr_cdx_details INTO lv_cdx_rec;
      IF cr_cdx_details%FOUND THEN
        UPDATE txnlifecycle tlc
        SET    cdxwfprocid       = lv_cdx_rec.wf_proc_id,
               cdxwfcreationdate = lv_cdx_rec.wf_creation_date,
               cdxfaid           = lv_cdx_rec.fa_id,
               cdxfacreationdate = lv_cdx_rec.fa_creation_date
        WHERE CURRENT OF cr_incomplete_txns;
        lv_record_count := lv_record_count + 1;
      END IF;
      CLOSE cr_cdx_details;
    END LOOP;  
    pr_update_run_history_step(
      prv_step => '30',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_cdx_new_wf_fa;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with status of CCB workflow and field activity
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_cdx_wf_fa_status IS
    lv_record_count INTEGER := 0;
    
  BEGIN
    pr_create_run_history_step(
      prv_step              => '35',
      prv_step_description  => 'CCB workflow and field activity current status');
    
    UPDATE txnlifecycle tlc
    SET    tlc.cdxwfstatus =
           (
             SELECT lok.descr
             FROM   cisadm.ci_wf_proc@cdx_link wf
             JOIN   cisadm.ci_lookup@cdx_link lok
                ON  lok.field_name = 'WF_STAT_RSN_FLG'
                AND wf.wf_stat_rsn_flg = lok.field_value
             WHERE  wf.wf_proc_id = tlc.cdxwfprocid
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxwfprocid IS NOT NULL;
    lv_record_count := SQL%ROWCOUNT;

    UPDATE txnlifecycle tlc
    SET    tlc.cdxfastatus =
           (
             SELECT lok.descr
             FROM   cisadm.ci_fa@cdx_link fa
             JOIN   cisadm.ci_lookup@cdx_link lok
                ON  lok.field_name = 'FA_STATUS_FLG'
                AND fa.fa_status_flg = lok.field_value
             WHERE  fa.fa_id = tlc.cdxfaid
           ),
           tlc.cdxfastagestatus =
           (
             SELECT fa_up_status_flg
             FROM   cisadm.ci_fa_stage_up@cdx_link fsu
             WHERE  fsu.fa_id = tlc.cdxfaid
           ),
           tlc.cdxfastageerror =
           (
             SELECT fse.exp_msg
             FROM   cisadm.ci_fa_stgup_exc@cdx_link fse
             WHERE  fse.fa_id = tlc.cdxfaid
             AND    fse.message_cat_nbr = '9'
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxfaid IS NOT NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;

    pr_update_run_history_step(
      prv_step => '35',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_cdx_wf_fa_status;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with date that Gas Meter Notification was sent to AEMO
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_cdx_gmntoaemo IS
    lv_record_count INTEGER := 0;

  BEGIN
    pr_create_run_history_step(
      prv_step              => '38',
      prv_step_description  => 'CCB GMN to AEMO date');
    
    UPDATE txnlifecycle tlc
    SET    tlc.cdxgmntoaemodate = (
                                    SELECT MAX(gmn.gmntransactiondate)
                                    FROM   gasmeternotification gmn
                                    WHERE  gmn.serviceorderno = tlc.wmserviceorder
                                  )  
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxgmntoaemodate IS NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;

    -- Update AEMO GMN where source view sordgasmeternotification does not have the required info
  	-- Source data from workflow context
  	UPDATE txnlifecycle tlc
  	SET  tlc.cdxgmntoaemodate =(
                                 SELECT MAX(closase.cre_dttm)
                                   FROM   cisadm.ci_wf_evt_ctxt@cdx_link sordclosure
                                   JOIN   cisadm.ci_nt_dwn@cdx_link closase
                                     ON   closase.nt_dwn_id = trim(sordclosure.ctxt_val)
                                  WHERE  sordclosure.wf_proc_id = tlc.cdxwfprocid
                                    AND    sordclosure.evt_seq = '550'
                                )
     WHERE  tlc.transactionprocessingcomplete = 'Y'
      AND    tlc.cdxgmntoaemodate IS NULL;
      lv_record_count := lv_record_count + SQL%ROWCOUNT;    
    
    pr_update_run_history_step(
      prv_step => '38',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_cdx_gmntoaemo;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with details of MXO service request and work order creation
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_mxo_new_sr_wo IS
    lv_record_count INTEGER := 0;
    
  BEGIN
    pr_create_run_history_step(
      prv_step              => '40',
      prv_step_description  => 'MXO service request and work order for service order');
    
    -- Update with service request number
    UPDATE txnlifecycle tlc
    SET    (mxosrnum, mxosrcreationdate) =
           (
             SELECT MAX(tkt.ticketid),
                    MAX(tkt.reportdate)                  
             FROM   maximo.ticketspec@mx7_link tspfa
             JOIN   maximo.ticket@mx7_link tkt
                ON  tkt.siteid = tspfa.siteid
                AND tkt.ticketid = tspfa.ticketid
             JOIN   maximo.ticketspec@mx7_link tspso
                ON  tspso.siteid = tkt.siteid
                AND tspso.ticketid = tkt.ticketid
             WHERE  tspfa.assetattrid = 'CDX'
             AND    tspfa.alnvalue = tlc.cdxfaid
             AND    tspso.assetattrid = 'RET_REF_NUM'
             AND    tspso.alnvalue = tlc.wmserviceorder
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.cdxfaid IS NOT NULL
    AND    tlc.mxosrnum IS NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;

    -- Update with work order number
    UPDATE txnlifecycle tlc
    SET    (mxowonum, mxowocreationdate) =
           (
             SELECT MAX(wo.wonum),
                    MAX(wo.reportdate)                  
             FROM   maximo.relatedrecord@mx7_link rr
             JOIN   maximo.workorder@mx7_link wo
                ON  wo.siteid = rr.siteid
                AND wo.wonum = rr.relatedreckey
             WHERE  rr.recordkey = mxosrnum
             AND    rr.class = 'SR'
             AND    rr.relatedrecclass = 'WORKORDER'
             AND    rr.relatetype = 'FOLLOWUP'
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.mxosrnum IS NOT NULL
    AND    tlc.mxowonum IS NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;

    -- Update work order status dates
    UPDATE txnlifecycle tlc
    SET    (mxowoactfinishdate, mxowocompletedstatusdate, mxowoclosedstatusdate) =
           (
             SELECT wo.actfinish,
                    (
                      SELECT MAX(changedate)
                      FROM   maximo.wostatus@mx7_link wos
                      WHERE  wos.siteid = wo.siteid
                      AND    wos.wonum = wo.wonum
                      AND    wos.status = 'COMP'
                    ) AS wo_completion_date,
                    (
                      SELECT MAX(changedate)
                      FROM   maximo.wostatus@mx7_link wos
                      WHERE  wos.siteid = wo.siteid
                      AND    wos.wonum = wo.wonum
                      AND    wos.status = 'COMP'
                    ) AS wo_closed_date
             FROM   maximo.workorder@mx7_link wo
             WHERE  wo.wonum = tlc.mxowonum
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.mxowonum IS NOT NULL;
    lv_record_count := lv_record_count + SQL%ROWCOUNT;

    pr_update_run_history_step(
      prv_step => '40',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_mxo_new_sr_wo;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders for which cancellation request was received
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_sord_cancellation IS
    lv_record_count INTEGER := 0;
    CURSOR cr_sord_cancellation IS
      SELECT src.gdndivision,
             src.transactiondate,
             src.serviceorderno,
             src.transactionid,
             src.jobenquirycode,
             atx.recipient,
             atx.sender
      FROM   sordrequestcancel src
      JOIN   asetxn atx
         ON  atx.gdndivision = src.gdndivision
         AND atx.docid = src.docid
         AND atx.messageid = src.messageid
      WHERE  src.transactiondate > pkv_sord_cutoff_date;
    CURSOR cr_txn (cv_gdndivision  VARCHAR2,
                   cv_service_order VARCHAR2) IS
      SELECT *
      FROM   txnlifecycle tlc
      WHERE  tlc.gdndivision = cv_gdndivision
      AND    tlc.wmserviceorder = cv_service_order
      FOR UPDATE OF wmcancellationtransactionid;
    lv_txn_rec cr_txn%ROWTYPE;

  BEGIN
    pr_create_run_history_step(
      prv_step              => '50',
      prv_step_description  => 'Cancellation of service order');
     
    FOR lv_cancel_rec IN cr_sord_cancellation LOOP
      OPEN cr_txn(cv_gdndivision   => lv_cancel_rec.gdndivision,
                  cv_service_order  => lv_cancel_rec.serviceorderno);

      FETCH cr_txn INTO lv_txn_rec;
      IF cr_txn%FOUND THEN
        UPDATE txnlifecycle tlc
        SET    tlc.wmcancellationtransactionid = lv_cancel_rec.transactionid,
               tlc.wmcancellationtransactiondate = lv_cancel_rec.transactiondate
        WHERE CURRENT OF cr_txn;
      ELSE
        INSERT INTO txnlifecycle(
          transactionprocessingcomplete,
          gdndivision,
          wmserviceorder,
          wmjobenquirycode,
          wmcancellationtransactionid,
          wmcancellationtransactiondate,
          wmrecipient,
          wmsender
        ) VALUES (
          'N',
          lv_cancel_rec.gdndivision,
          lv_cancel_rec.serviceorderno,
          lv_cancel_rec.jobenquirycode,
          lv_cancel_rec.transactionid,
          lv_cancel_rec.transactiondate,
          lv_cancel_rec.recipient,
          lv_cancel_rec.sender
         );
      END IF;
      CLOSE cr_txn;
      lv_record_count := lv_record_count + 1;
    END LOOP;  

    pr_update_run_history_step(
      prv_step => '50',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_sord_cancellation;

   ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with date that closure response was sent to retailer
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_sord_closure_response IS
    lv_record_count INTEGER := 0;
  BEGIN
    pr_create_run_history_step(
      prv_step              => '60',
      prv_step_description  => 'Closure response on service order');
     
    UPDATE txnlifecycle tlc
    SET    wmclosureresponsedate = 
           (
             SELECT MAX(transactiondate)
             FROM   sordresponseclosure src
             WHERE  src.gdndivision = tlc.gdndivision
             AND    src.serviceorderno = tlc.wmserviceorder
           )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.wmserviceorder IS NOT NULL;

    
    -- Where SORDRESPONSECLOSURE do not have the required value, perform an update where transaction date can be derived by getting
    -- actual NDS ID and linking to ASETXN
    UPDATE txnlifecycle tlc
    SET    wmclosureresponsedate = 
    	   (
    	     SELECT MAX(closase.cre_dttm)
    	     FROM   cisadm.ci_wf_evt_ctxt@cdx_link sordclosure
             JOIN   cisadm.ci_wf_evt@cdx_link evt
                ON  evt.wf_proc_id = sordclosure.wf_proc_id
                AND evt.evt_seq = sordclosure.evt_seq
    	     JOIN   cisadm.ci_nt_dwn@cdx_link closase
    	        ON  closase.nt_dwn_id = trim(sordclosure.ctxt_val)
    	     WHERE  sordclosure.wf_proc_id = tlc.cdxwfprocid 
    	     AND    evt.wf_evt_type_cd IN ('CMDLSOCLRSP1','CMDLSOCLRSP2')
    	   )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.wmserviceorder IS NOT NULL    	   
    AND    tlc.wmclosureresponsedate IS NULL;    
    
    -- Perform a second update to calculate the number of working days that have elapsed from the actual finish date defined
    -- on the work order to the date that the closure response was sent to the retailer
    UPDATE txnlifecycle tlc
    SET    finishtoclosureresponsedays = GREATEST(NEXT_DAY(mxowoactfinishdate,'MON') - mxowoactfinishdate - 2,0) +
                                         (NEXT_DAY(wmclosureresponsedate,'MON') - NEXT_DAY(mxowoactfinishdate,'MON'))/7*5 - 
                                         GREATEST(NEXT_DAY(wmclosureresponsedate,'MON') - wmclosureresponsedate - 3,0) -
                                         (
                                           SELECT COUNT(*)
                                           FROM   maximo.workperiod@mx7_link wp
                                           JOIN   gdn_division_xref gdx
                                              ON  gdx.calnum = wp.calnum
                                           WHERE  gdx.gdndivision = tlc.gdndivision
                                           AND    wp.workdate BETWEEN TRUNC(tlc.mxowoactfinishdate) AND TRUNC(tlc.wmclosureresponsedate)
                                           AND    wp.shiftnum = 'HOLIDAY'
                                         )
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.mxowoactfinishdate IS NOT NULL
    AND    tlc.wmclosureresponsedate IS NOT NULL;    

    -- Perform a third update to indicate that processing is complete on service orders that
    -- have had their closure response date populated.  This prevents further processing of these
    -- service orders in subsequent runs
    UPDATE txnlifecycle tlc
    SET    transactionprocessingcomplete = 'Y'
    WHERE  tlc.transactionprocessingcomplete = 'N'
    AND    tlc.wmserviceorder IS NOT NULL
    AND    tlc.wmclosureresponsedate IS NOT NULL;    
    lv_record_count := SQL%ROWCOUNT;
    pr_update_run_history_step(
      prv_step => '60',
      prv_record_count => lv_record_count);
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_sord_closure_response;

 ------------------------------------------------------------------------------------------------------------------
  -- Main controlling procedure for population process
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE main IS
  BEGIN
    pkv_txnpopid := fn_create_run_history;
    pr_insert_new_sords;
    pr_insert_new_gmns;
    pr_gmn_update;
    pr_sord_rejected;
    pr_sord_initial_response;
    pr_cdx_new_wf_fa;
    pr_cdx_wf_fa_status;
    pr_cdx_gmntoaemo;
    pr_mxo_new_sr_wo;
    pr_sord_cancellation;
    pr_sord_closure_response;
    pr_update_run_history(
      prv_status => 'COMPLETE',
      prv_enddate => SYSDATE);
  END main;

END populate_txnlifecycle;
/

SHOW ERRORS
/