
----------------------------------------------------------------------------
-- DMND0002592 
-- Missing transactions information could not be sourced from source systems
----------------------------------------------------------------------------
UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='19/09/2016 12:47:18'
WHERE tlc.wmserviceorder = '7424919541'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='08/07/2016 17:14:23', cdxgmntoaemodate='08/07/2016 17:14:23'
where tlc.wmserviceorder = '2002119847'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='08/07/2016 17:14:13', cdxgmntoaemodate='08/07/2016 17:14:13'
where tlc.wmserviceorder = '2408538'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='08/07/2016 17:14:23'
where tlc.wmserviceorder = '6502170131'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='08/07/2016 17:14:23'
where tlc.wmserviceorder = '4170330356'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='08/07/2016 17:14:13'
where tlc.wmserviceorder = '0411379223'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='06/07/2016 17:14:02', cdxgmntoaemodate='06/07/2016 17:14:02'
where tlc.wmserviceorder = '2002110653'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='08/07/2016 17:14:13', cdxgmntoaemodate='08/07/2016 17:14:13'
where tlc.wmserviceorder = '2402623'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='06/07/2016 17:14:12', cdxgmntoaemodate='06/07/2016 17:14:12'
where tlc.wmserviceorder = '2002110167'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='06/07/2016 17:14:22', cdxgmntoaemodate='06/07/2016 17:14:22'
where tlc.wmserviceorder = '2002109130'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='01/09/2015 16:50:26', cdxgmntoaemodate='01/09/2015 16:51:26'
where tlc.wmserviceorder = '0313817324'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='09/09/2015 16:16:57'
where tlc.wmserviceorder = '6714811873'
/

UPDATE txnlifecycle tlc
SET    wmclosureresponsedate='14/07/2015 13:40:38', cdxgmntoaemodate='14/07/2015 13:40:38'
where tlc.wmserviceorder = '2001582844'
/
