ALTER TABLE tm.txnlifecycle ADD (
  cdxfastageerror              VARCHAR2(254)
)
/

UPDATE txnlifecycle tlc
SET    cdxfastageerror =
       (
         SELECT fse.exp_msg
         FROM   cisadm.ci_fa_stgup_exc@cdx_link fse
         WHERE  fse.fa_id = tlc.cdxfaid
         AND    fse.message_cat_nbr = '9'
       )
WHERE tlc.cdxfaid IN
       (
         SELECT fse.fa_id
         FROM   cisadm.ci_fa_stgup_exc@cdx_link fse
         WHERE   fse.message_cat_nbr = '9'
       )
/

UPDATE gasmeternotification gmn
SET gmnmirn = (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = SUBSTR(gmn.gmntransactionid,INSTR(gmn.gmntransactionid,'Txn')+4)
               AND    ctxt.nt_dwn_ctxt_flg = 'NMI'
             ),  
    gmnmirnstatus = (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = SUBSTR(gmn.gmntransactionid,INSTR(gmn.gmntransactionid,'Txn')+4)
               AND    ctxt.nt_dwn_ctxt_flg = 'MSTS'
             ),
    serviceorderno = (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = SUBSTR(gmn.gmntransactionid,INSTR(gmn.gmntransactionid,'Txn')+4)
               AND    ctxt.nt_dwn_ctxt_flg = 'SONB'
             )
WHERE  serviceorderno IS NULL
/

UPDATE txnlifecycle tlc
SET    tlc.cdxgmntoaemodate = (
                                SELECT MAX(gmn.gmntransactiondate)
                                FROM   gasmeternotification gmn
                                WHERE  gmn.serviceorderno = tlc.wmserviceorder
                              )  
WHERE  tlc.cdxgmntoaemodate IS NULL
/

