CREATE OR REPLACE PACKAGE gmn_oneoff_update AS

  PROCEDURE main;

END gmn_oneoff_update;
/

CREATE OR REPLACE PACKAGE BODY gmn_oneoff_update AS  

  PROCEDURE pr_insert_new_gmns IS
    CURSOR cr_gmns IS
      SELECT *
      FROM   sordgasmeternotification sordgmn
      WHERE  sordgmn.transactiondate >=  TO_DATE('1/1/2015','DD/MM/YYYY')
      AND NOT EXISTS
             (
                SELECT 'x'
                FROM   gasmeternotification gmn
                WHERE  gmn.gdndivision = sordgmn.gdndivision
                AND    gmn.gmntransactionid = sordgmn.transactionid
             );

    CURSOR cr_asetxn(ci_gdndivision VARCHAR2,
                     ci_docid       VARCHAR2,
                     ci_messageid   VARCHAR2)
    IS
      SELECT *
      FROM   asetxn
      WHERE  gdndivision = ci_gdndivision
      AND    docid = ci_docid
      AND    messageid = ci_messageid;

    CURSOR cr_receipt(ci_gdndivision   VARCHAR2,
                      ci_transactionid VARCHAR2)
    IS
      SELECT docid,
             messageid,
             initiatingtransactionid,
             gdndivision,
             'Accept' AS receipttype,
             receiptid,
             receiptdate,
             NULL AS eventclass,
             NULL AS eventcode,
             NULL AS eventcodedesc,
             NULL AS eventcontext,
             NULL AS eventexplanation,
             NULL AS eventkeyinfo,
             NULL AS eventseverity
      FROM   asetxnackaccept
      WHERE  gdndivision = ci_gdndivision
      AND    initiatingtransactionid = ci_transactionid
      UNION ALL
      SELECT docid,
             messageid,
             initiatingtransactionid,
             gdndivision,
             'Partial' AS receipttype,
             receiptid,
             receiptdate,
             eventclass,
             eventcode,
             eventcodedesc,
             eventcontext,
             eventexplanation,
             eventkeyinfo,
             eventseverity
      FROM   asetxnackpartial
      WHERE  gdndivision = ci_gdndivision
      AND    initiatingtransactionid = ci_transactionid
      UNION ALL
      SELECT docid,
             messageid,
             initiatingtransactionid,
             gdndivision,
             'Reject' AS receipttype,
             receiptid,
             receiptdate,
             eventclass,
             eventcode,
             eventcodedesc,
             eventcontext,
             eventexplanation,
             eventkeyinfo,
             eventseverity
      FROM   asetxnackreject
      WHERE  gdndivision = ci_gdndivision
      AND    initiatingtransactionid = ci_transactionid;

    CURSOR cr_ccbdwn(ci_transactionid VARCHAR2)
    IS
      SELECT (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = dwn.nt_dwn_id
               AND    ctxt.nt_dwn_ctxt_flg = 'NMI'
             ) AS mirn,  
             (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = dwn.nt_dwn_id
               AND    ctxt.nt_dwn_ctxt_flg = 'MSTS'
             ) AS mirnstatus,
             (
               SELECT LTRIM(RTRIM(ctxt_val))
               FROM   cisadm.ci_nt_dwn_ctxt@cdx_link ctxt
               WHERE  ctxt.nt_dwn_id = dwn.nt_dwn_id
               AND    ctxt.nt_dwn_ctxt_flg = 'SONB'
             ) AS serviceorderno
      FROM   cisadm.ci_nt_dwn@cdx_link dwn
      WHERE  nt_dwn_id = ci_transactionid;

    lv_asetxn_rec asetxn%ROWTYPE;
    lv_gasmeternotification_rec gasmeternotification%ROWTYPE;
    lv_receipt_rec cr_receipt%ROWTYPE;
    lv_ccbdwn_rec cr_ccbdwn%ROWTYPE;
    
  BEGIN
    FOR lv_gmn_rec IN cr_gmns LOOP
      -- Get ASETXN record for GMN
      OPEN cr_asetxn(ci_gdndivision => lv_gmn_rec.gdndivision,
                     ci_docid       => lv_gmn_rec.docid,
                     ci_messageid   => lv_gmn_rec.messageid);
      FETCH cr_asetxn INTO lv_asetxn_rec;
      CLOSE cr_asetxn;
      lv_gasmeternotification_rec.gdndivision := lv_gmn_rec.gdndivision;
      lv_gasmeternotification_rec.gmndocid := lv_gmn_rec.docid;
      lv_gasmeternotification_rec.gmnmessageid := lv_gmn_rec.messageid;
      lv_gasmeternotification_rec.gmntxnmessagedate := lv_asetxn_rec.messagedate;
      lv_gasmeternotification_rec.gmntxnpriority := lv_asetxn_rec.priority;
      lv_gasmeternotification_rec.gmntxnrecipient := lv_asetxn_rec.recipient;
      lv_gasmeternotification_rec.gmntxnsender := lv_asetxn_rec.sender;
      lv_gasmeternotification_rec.gmntransactionid := lv_gmn_rec.transactionid;
      lv_gasmeternotification_rec.gmntransactiondate := lv_gmn_rec.transactiondate;
      lv_gasmeternotification_rec.gmnordercompleteddate  := lv_gmn_rec.ordercompleteddate;

      -- Get receipt for GMN
      OPEN cr_receipt(ci_gdndivision   => lv_gmn_rec.gdndivision,
                      ci_transactionid => lv_gmn_rec.transactionid);
      FETCH cr_receipt INTO lv_receipt_rec;
      CLOSE cr_receipt;
      lv_gasmeternotification_rec.rctreceipttype := lv_receipt_rec.receipttype;
      lv_gasmeternotification_rec.rctdocid := lv_receipt_rec.docid;
      lv_gasmeternotification_rec.rctmessageid := lv_receipt_rec.messageid;
      lv_gasmeternotification_rec.rctreceiptid := lv_receipt_rec.receiptid;
      lv_gasmeternotification_rec.rctreceiptdate := lv_receipt_rec.receiptdate;
      lv_gasmeternotification_rec.rcteventclass := lv_receipt_rec.eventclass;
      lv_gasmeternotification_rec.rcteventcode := lv_receipt_rec.eventcode;
      lv_gasmeternotification_rec.rcteventcodedesc := lv_receipt_rec.eventcodedesc;
      lv_gasmeternotification_rec.rcteventexplanation := lv_receipt_rec.eventexplanation;
      lv_gasmeternotification_rec.rcteventkeyinfo := lv_receipt_rec.eventkeyinfo;
      lv_gasmeternotification_rec.rcteventseverity := lv_receipt_rec.eventseverity;

      -- Get CCB details for GMN
      lv_ccbdwn_rec := NULL;
      OPEN cr_ccbdwn(ci_transactionid => SUBSTR(lv_gmn_rec.transactionid,14,12));
      FETCH cr_ccbdwn INTO lv_ccbdwn_rec;
      CLOSE cr_ccbdwn;
     
      lv_gasmeternotification_rec.gmnmirn := COALESCE(lv_gmn_rec.mirn,lv_ccbdwn_rec.mirn);
      lv_gasmeternotification_rec.gmnmirnstatus := COALESCE(lv_gmn_rec.mirnstatus,lv_ccbdwn_rec.mirnstatus);
      lv_gasmeternotification_rec.serviceorderno := lv_ccbdwn_rec.serviceorderno;

      INSERT INTO gasmeternotification(
        gdndivision,
        gmndocid,
        gmnmessageid,
        gmntxnmessagedate,
        gmntxnpriority,
        gmntxnrecipient,
        gmntxnsender,
        gmntransactionid,
        gmntransactiondate,
        gmnmirn,
        gmnmirnstatus,
        gmnordercompleteddate,
        rctreceipttype,
        rctdocid,
        rctmessageid,
        rctreceiptid,
        rctreceiptdate,
        rcteventclass,
        rcteventcode,
        rcteventcodedesc,
        rcteventexplanation,
        rcteventkeyinfo,
        rcteventseverity,
        serviceorderno
      ) VALUES (
        lv_gasmeternotification_rec.gdndivision,
        lv_gasmeternotification_rec.gmndocid,
        lv_gasmeternotification_rec.gmnmessageid,
        lv_gasmeternotification_rec.gmntxnmessagedate,
        lv_gasmeternotification_rec.gmntxnpriority,
        lv_gasmeternotification_rec.gmntxnrecipient,
        lv_gasmeternotification_rec.gmntxnsender,
        lv_gasmeternotification_rec.gmntransactionid,
        lv_gasmeternotification_rec.gmntransactiondate,
        lv_gasmeternotification_rec.gmnmirn,
        lv_gasmeternotification_rec.gmnmirnstatus,
        lv_gasmeternotification_rec.gmnordercompleteddate,
        lv_gasmeternotification_rec.rctreceipttype,
        lv_gasmeternotification_rec.rctdocid,
        lv_gasmeternotification_rec.rctmessageid,
        lv_gasmeternotification_rec.rctreceiptid,
        lv_gasmeternotification_rec.rctreceiptdate,
        lv_gasmeternotification_rec.rcteventclass,
        lv_gasmeternotification_rec.rcteventcode,
        lv_gasmeternotification_rec.rcteventcodedesc,
        lv_gasmeternotification_rec.rcteventexplanation,
        lv_gasmeternotification_rec.rcteventkeyinfo,
        lv_gasmeternotification_rec.rcteventseverity,
        lv_gasmeternotification_rec.serviceorderno
      );
    END LOOP;

  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_insert_new_gmns;

  ------------------------------------------------------------------------------------------------------------------
  -- Update service orders with date that Gas Meter Notification was sent to AEMO
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_cdx_gmntoaemo IS
  BEGIN
    UPDATE txnlifecycle tlc
    SET    tlc.cdxgmntoaemodate = (
                                    SELECT MAX(gmn.gmntransactiondate)
                                    FROM   gasmeternotification gmn
                                    WHERE  gmn.serviceorderno = tlc.wmserviceorder
                                  )  
    WHERE  tlc.cdxgmntoaemodate IS NULL;
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END pr_cdx_gmntoaemo;

  PROCEDURE main IS
  BEGIN
    pr_insert_new_gmns;
    pr_cdx_gmntoaemo;
  END main;

END gmn_oneoff_update;
/

SHOW ERRORS
/

EXEC gmn_oneoff_update.main;

BEGIN
  dbms_scheduler.create_job(job_name  => 'job_tm_gmn_oneoff_update',
                            job_type   => 'STORED_PROCEDURE',
                            job_action => 'tm.gmn_oneoff_update.main',
                            start_date => SYSDATE,
                            end_date   => NULL,
                            job_class  => 'DEFAULT_JOB_CLASS',
                            enabled    => TRUE,
                            auto_drop  => TRUE,
                            comments   => NULL);
END;
/

DROP PACKAGE gmn_oneoff_update
/
