--
-- To be run as a DBA
--

SET SERVEROUTPUT ON

DECLARE
  CURSOR cr_tm_tables IS
    SELECT *
    FROM   all_tables
    WHERE  owner IN ('TM_SA','TM_VIC','TM_QLD','TM_MAN', 'TM_NSWT', 'TM_NSWW', 'TM_WM')
    AND    table_name IN (
                          'ASETXN',
                          'ASETXNACKACCEPT',
                          'ASETXNACKPARTIAL',
                          'ASETXNACKREJECT',
                          'CATSCHANGEREQUEST',
                          'CATSCHANGERESPONSE',
                          'CATSDATAREQUEST',
                          'CATSNOTIFICATION',
                          'CUSTCUSTOMERDETAILSNOTIFY',
                          'EBTXN',
                          'EBTXNACKNOWLEDGMENT',
                          'EBTXNMESSAGEERROR',
                          'MDMTACCOUNTCREATIONNOTIFY',
                          'MDMTMETERDATAMISSINGNOTIFY',
                          'MDMTMETERDATANOTIFICATION',
                          'MDMTMETERDATARESPONSE',
                          'MDMTMETERDATAVERIFYREQUEST',
                          'MDMTMETERDATAVERIFYRESPONSE',
                          'MDMTMETEREDSUPPLYUPDATE',
                          'MDMTMETERREADINPUTNOTIFICATION',
                          'MDMTSPECIALREADREQUEST',
                          'MDMTSPECIALREADRESPONSE',
                          'NETBDUOSBILLINGNOTIFICATION',
                          'NMIDISCOVERYREQUEST',
                          'NMIDISCOVERYRESPONSE',
                          'NMISTANDINGDATAREQUEST',
                          'NMISTANDINGDATARESPONSE',
                          'NMISTANDINGDATAUPDATENOTIFY',
                          'SITEAMENDMETERROUTEDETAILS',
                          'SORDGASMETERNOTIFICATION',
                          'SORDREQUESTCANCEL',
                          'SORDREQUESTNEW',
                          'SORDRESPONSECLOSURE',
                          'SORDRESPONSEINITIAL',
                          'WMCAPTURELOG'
                        )
    ORDER BY table_name, owner;
BEGIN
  FOR lv_table_rec IN cr_tm_tables LOOP
    -- Grant select access on all tables to readonly user
    BEGIN
      DBMS_OUTPUT.PUT_LINE('GRANT SELECT ON '|| LOWER(lv_table_rec.owner || '.' || lv_table_rec.table_name) || ' TO readonly, tm');
      EXECUTE IMMEDIATE 'GRANT SELECT ON '|| LOWER(lv_table_rec.owner || '.' || lv_table_rec.table_name) || ' TO readonly, tm';
      DBMS_OUTPUT.PUT_LINE('GRANT SUCCEEDED');
    EXCEPTION  
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('GRANT FAILED: ' || SQLERRM);
    END;   
    -- When processing WM instance of table, grant access to TM owned view to readonly user
    IF lv_table_rec.owner = 'TM_WM' THEN
      BEGIN
        DBMS_OUTPUT.PUT_LINE('GRANT SELECT ON tm.' || LOWER(lv_table_rec.table_name) || ' TO readonly');
        EXECUTE IMMEDIATE 'GRANT SELECT ON tm.' || LOWER(lv_table_rec.table_name) || ' TO readonly';
        DBMS_OUTPUT.PUT_LINE('GRANT SUCCEEDED');
      EXCEPTION  
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('GRANT FAILED: ' || SQLERRM);
      END;   
    END IF;
  END LOOP;
END;
/
