CREATE TABLE tm.db_gasmeternotification (
  gdndivision                   VARCHAR2(4) NOT NULL,
  gmndocid                      VARCHAR2(64),
  gmnmessageid                  VARCHAR2(64),
  gmntxnmessagedate             DATE,
  gmntxnpriority                VARCHAR2(64),
  gmntxnrecipient               VARCHAR2(64),
  gmntxnsender                  VARCHAR2(64),
  gmntransactionid              VARCHAR2(64),
  gmntransactiondate            DATE,
  gmnmirn                       VARCHAR2(64),
  gmnmirnstatus                 VARCHAR2(64),
  gmnordercompleteddate         DATE,
  rctreceipttype                VARCHAR2(64),
  rctdocid                      VARCHAR2(64),
  rctmessageid                  VARCHAR2(64),
  rctreceiptid                  VARCHAR2(64),
  rctreceiptdate                DATE,
  rcteventclass                 VARCHAR2(64),
  rcteventcode                  VARCHAR2(4),
  rcteventcodedesc              VARCHAR2(512),
  rcteventexplanation           VARCHAR2(512),
  rcteventkeyinfo               VARCHAR2(64),
  rcteventseverity              VARCHAR2(64),
  cdxfaid                       VARCHAR2(10),
  mxowonum                      VARCHAR2(10)
)
/

CREATE INDEX tm.db_gasmeternotification_idx1 
ON tm.db_gasmeternotification (gdndivision, gmndocid, gmnmessageid)
/

CREATE INDEX tm.db_gasmeternotification_idx2
ON tm.db_gasmeternotification (gdndivision, gmntransactionid)
/

CREATE INDEX tm.db_gasmeternotification_idx3
ON tm.db_gasmeternotification (gmnmirn)
/

CREATE INDEX tm.db_gasmeternotification_idx4
ON tm.db_gasmeternotification (gdndivision, cdxfaid)
/

CREATE INDEX tm.db_gasmeternotification_idx5
ON tm.db_gasmeternotification (rctreceiptdate)
/

CREATE INDEX tm.db_gasmeternotification_idx6
ON tm.db_gasmeternotification (gdndivision, cdxfaid, mxowonum)
/

GRANT SELECT ON tm.db_gasmeternotification TO readonly
/
