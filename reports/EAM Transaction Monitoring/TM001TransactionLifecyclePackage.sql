CREATE OR REPLACE PACKAGE tm.tm_txn_lifecycle AS

  TYPE ty_txn_lifecycle_rec IS RECORD (
    row_sequence     INTEGER,
    txn_date         DATE,
    wm_docid         VARCHAR2(100),
    wm_messageid     VARCHAR2(100),
    wm_transactionid VARCHAR2(100),
    wm_service_order VARCHAR2(100),
    cdx_wf_proc_id   VARCHAR2(100),
    cdx_fa_id        VARCHAR2(100),
    mxo_srnum        VARCHAR2(100),
    mxo_wonum        VARCHAR2(100),
    errwarn          VARCHAR2(1),
    comments         VARCHAR2(1000)
  );
  TYPE ty_txn_lifecycle IS TABLE OF ty_txn_lifecycle_rec;
  
  -- Procedure used to initailise package variables
  -- Used when accessing main fn_txn_lifecycle function via the tm_txn_lifecycle view
  -- This mechanism is required to support remote access of the function from another database
  PROCEDURE pr_txn_lifecycle(
    prv_originating_txn VARCHAR2 DEFAULT NULL,
    prv_service_order   VARCHAR2 DEFAULT NULL,
    prv_field_activity  VARCHAR2 DEFAULT NULL,
    prv_service_request VARCHAR2 DEFAULT NULL,
    prv_work_order      VARCHAR2 DEFAULT NULL);

  FUNCTION fn_txn_lifecycle
  RETURN ty_txn_lifecycle PIPELINED;

END tm_txn_lifecycle;
/

SHOW ERRORS
/

CREATE OR REPLACE PACKAGE BODY tm.tm_txn_lifecycle AS
  pkv_originating_txn VARCHAR2(100);
  pkv_service_order   VARCHAR2(50);
  pkv_field_activity  VARCHAR2(50);
  pkv_service_request VARCHAR2(50);
  pkv_work_order      VARCHAR2(50);
  pkv_row_sequence    INTEGER;
  pkv_eb_asn_txn      ty_txn_lifecycle := ty_txn_lifecycle();
  TYPE ty_refcur IS REF CURSOR;

  ------------------------------------------------------------------------------------------------------------------
  -- Initialise package variables to the parameters passed from the BIRT report
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_txn_lifecycle(
    prv_originating_txn VARCHAR2 DEFAULT NULL,
    prv_service_order   VARCHAR2 DEFAULT NULL,
    prv_field_activity  VARCHAR2 DEFAULT NULL,
    prv_service_request VARCHAR2 DEFAULT NULL,
    prv_work_order      VARCHAR2 DEFAULT NULL)
  IS
  BEGIN
    -- Force all package variables to null and then reinitialise with any parameters set
    pkv_originating_txn := NULL;
    pkv_service_order   := NULL;
    pkv_field_activity  := NULL;
    pkv_service_request := NULL;
    pkv_work_order      := NULL;
    
    pkv_originating_txn := prv_originating_txn;
    pkv_service_order   := prv_service_order;
    pkv_field_activity  := prv_field_activity;
    pkv_service_request := prv_service_request;
    pkv_work_order      := prv_work_order;
    pkv_row_sequence    := 0;

    -- Clear the report contents from any previous report call
    IF pkv_eb_asn_txn.COUNT > 0 THEN
      pkv_eb_asn_txn.DELETE;
    END IF;
  END pr_txn_lifecycle;
    
  ------------------------------------------------------------------------------------------------------------------
  -- Add a row to be reported
  ------------------------------------------------------------------------------------------------------------------
  PROCEDURE pr_txn_lifecycle_rec(
    prv_txn_date         DATE     DEFAULT NULL,
    prv_wm_docid         VARCHAR2 DEFAULT NULL,
    prv_wm_messageid     VARCHAR2 DEFAULT NULL,
    prv_wm_transactionid VARCHAR2 DEFAULT NULL,
    prv_wm_service_order VARCHAR2 DEFAULT NULL,
    prv_cdx_wf_proc_id   VARCHAR2 DEFAULT NULL,
    prv_cdx_fa_id        VARCHAR2 DEFAULT NULL,
    prv_mxo_srnum        VARCHAR2 DEFAULT NULL,
    prv_mxo_wonum        VARCHAR2 DEFAULT NULL,
    prv_errwarn          VARCHAR2 DEFAULT NULL,
    prv_comments         VARCHAR2 DEFAULT NULL)
  IS
  BEGIN
    pkv_row_sequence := pkv_row_sequence + 1;
    pkv_eb_asn_txn.EXTEND;
    pkv_eb_asn_txn(pkv_row_sequence).row_sequence := pkv_row_sequence;
    pkv_eb_asn_txn(pkv_row_sequence).txn_date := prv_txn_date;
    pkv_eb_asn_txn(pkv_row_sequence).wm_docid := prv_wm_docid;
    pkv_eb_asn_txn(pkv_row_sequence).wm_messageid := prv_wm_messageid;
    pkv_eb_asn_txn(pkv_row_sequence).wm_transactionid := prv_wm_transactionid;
    pkv_eb_asn_txn(pkv_row_sequence).wm_service_order := prv_wm_service_order;
    pkv_eb_asn_txn(pkv_row_sequence).cdx_wf_proc_id := prv_cdx_wf_proc_id;
    pkv_eb_asn_txn(pkv_row_sequence).cdx_fa_id := prv_cdx_fa_id;
    pkv_eb_asn_txn(pkv_row_sequence).mxo_srnum := prv_mxo_srnum;
    pkv_eb_asn_txn(pkv_row_sequence).mxo_wonum := prv_mxo_wonum;
    pkv_eb_asn_txn(pkv_row_sequence).errwarn := prv_errwarn;
    pkv_eb_asn_txn(pkv_row_sequence).comments := prv_comments;
  END pr_txn_lifecycle_rec;


  PROCEDURE pr_eb_ase_txn(
    prv_wm_docid          VARCHAR2 DEFAULT NULL,
    prv_wm_messageid      VARCHAR2 DEFAULT NULL,
    prv_wm_transactionid  VARCHAR2 DEFAULT NULL,
    prv_wm_service_order  VARCHAR2 DEFAULT NULL)
  IS
    lv_wm_ebtxn_cur       ty_refcur;
    lv_wm_ebtxn_rec       ebtxn%ROWTYPE;
    lv_wm_ebtxnack_cur    ty_refcur;
    lv_wm_ebtxnack_rec    ebtxnacknowledgment%ROWTYPE;
    lv_wm_ebtxnnack_cur   ty_refcur;
    lv_wm_ebtxnnack_rec   ebtxnmessageerror%ROWTYPE;
    lv_wm_asetxn_cur      ty_refcur;
    lv_wm_asetxn_rec      asetxn%ROWTYPE;
    lv_wm_asetxnack_cur   ty_refcur;
    lv_wm_asetxnack_rec   asetxnackaccept%ROWTYPE;
    lv_wm_asetxnnack_cur  ty_refcur;
    lv_wm_asetxnnack_rec  asetxnackreject%ROWTYPE;
    lv_wm_asetxnpart_cur  ty_refcur;
    lv_wm_asetxnpart_rec  asetxnackpartial%ROWTYPE;
  BEGIN

    ---- EBTXN processing ----
    OPEN lv_wm_ebtxn_cur FOR 'SELECT * FROM ebtxn WHERE  docid = :docid' USING prv_wm_docid;
    FETCH lv_wm_ebtxn_cur INTO lv_wm_ebtxn_rec;
    IF lv_wm_ebtxn_cur%FOUND THEN
      pr_txn_lifecycle_rec(
        prv_txn_date => lv_wm_ebtxn_rec.doctimestamp,
        prv_wm_docid => lv_wm_ebtxn_rec.docid,
        prv_comments => 'EBTXN - NativeId:' || lv_wm_ebtxn_rec.nativeid || ' UserStatus:' || lv_wm_ebtxn_rec.userstatus);

      -- Was EBTXN acknowledged
      OPEN lv_wm_ebtxnack_cur FOR 'SELECT * FROM ebtxnacknowledgment WHERE refmessageid = :refmessageid' USING lv_wm_ebtxn_rec.nativeid;
      FETCH lv_wm_ebtxnack_cur INTO lv_wm_ebtxnack_rec;
      IF lv_wm_ebtxnack_cur%FOUND THEN
        pr_txn_lifecycle_rec(
          prv_wm_docid => lv_wm_ebtxnack_rec.docid,
          prv_comments => 'EBTXN Acknowledged - RefMessageId:' || lv_wm_ebtxnack_rec.refmessageid);
      ELSE
        OPEN lv_wm_ebtxnnack_cur FOR 'SELECT * FROM ebtxnmessageerror WHERE refmessageid = :refmessageid' USING lv_wm_ebtxn_rec.nativeid;
        FETCH lv_wm_ebtxnnack_cur INTO lv_wm_ebtxnnack_rec;
        IF lv_wm_ebtxnnack_cur%FOUND THEN
          pr_txn_lifecycle_rec(
            prv_wm_docid => lv_wm_ebtxnnack_rec.docid,
            prv_errwarn  => 'W',
            prv_comments => 'EBTXN Not Acknowledged -  - RefMessageId:' || lv_wm_ebtxnnack_rec.refmessageid || ' ErrorMessage:' || lv_wm_ebtxnnack_rec.errormessage);
        ELSE
          pr_txn_lifecycle_rec(
            prv_errwarn  => 'W',
            prv_comments => 'Unable to find ACK or NACK for EBTXN');
        END IF;
        CLOSE lv_wm_ebtxnnack_cur;
      END IF;
      CLOSE lv_wm_ebtxnack_cur;
            
      ---- ASETXN processing ----
      OPEN lv_wm_asetxn_cur FOR 'SELECT * FROM asetxn WHERE  docid = :docid' USING prv_wm_docid;
      FETCH lv_wm_asetxn_cur INTO lv_wm_asetxn_rec;
      IF lv_wm_asetxn_cur%FOUND THEN
        pr_txn_lifecycle_rec(
          prv_txn_date     => lv_wm_asetxn_rec.messagedate,
          prv_wm_docid     => lv_wm_asetxn_rec.docid,
          prv_wm_messageid => lv_wm_asetxn_rec.messageid,
          prv_comments     => 'ASETXN - Priority:' || lv_wm_asetxn_rec.priority || ' Sender:' || lv_wm_asetxn_rec.sender || ' Recipient:' || lv_wm_asetxn_rec.recipient);
                
        -- Was ASETXN acknowledged
        OPEN lv_wm_asetxnack_cur FOR 'SELECT * FROM asetxnackaccept WHERE initiatingtransactionid = :transactionid' USING prv_wm_transactionid;
        FETCH lv_wm_asetxnack_cur INTO lv_wm_asetxnack_rec;
        IF lv_wm_asetxnack_cur%FOUND THEN
          pr_txn_lifecycle_rec(
            prv_txn_date         => lv_wm_asetxnack_rec.receiptdate,
            prv_wm_docid         => lv_wm_asetxnack_rec.docid,
            prv_wm_messageid     => lv_wm_asetxnack_rec.messageid,
            prv_wm_transactionid => lv_wm_asetxnack_rec.receiptid,
            prv_comments         => 'ASETXN Acknowledged');
        ELSE
          OPEN lv_wm_asetxnnack_cur FOR 'SELECT * FROM asetxnackreject WHERE initiatingtransactionid = :transactionid' USING prv_wm_transactionid;
          FETCH lv_wm_asetxnnack_cur INTO lv_wm_asetxnnack_rec;
          IF lv_wm_asetxnnack_cur%FOUND THEN
            pr_txn_lifecycle_rec(
              prv_txn_date         => lv_wm_asetxnnack_rec.receiptdate,
              prv_wm_docid         => lv_wm_asetxnnack_rec.docid,
              prv_wm_messageid     => lv_wm_asetxnnack_rec.messageid,
              prv_wm_transactionid => lv_wm_asetxnnack_rec.receiptid,
              prv_errwarn          => 'W',
              prv_comments         => 'ASETXN Rejected - EventCodeDesc:' || lv_wm_asetxnnack_rec.eventcodedesc || ' EventContext:' || lv_wm_asetxnnack_rec.eventcontext);
          ELSE
            OPEN lv_wm_asetxnpart_cur FOR 'SELECT * FROM asetxnackpartial WHERE initiatingtransactionid = :transactionid' USING prv_wm_transactionid;
            FETCH lv_wm_asetxnpart_cur INTO lv_wm_asetxnpart_rec;
            IF lv_wm_asetxnpart_cur%FOUND THEN
              pr_txn_lifecycle_rec(
                prv_txn_date         => lv_wm_asetxnpart_rec.receiptdate,
                prv_wm_docid         => lv_wm_asetxnpart_rec.docid,
                prv_wm_messageid     => lv_wm_asetxnpart_rec.messageid,
                prv_wm_transactionid => lv_wm_asetxnpart_rec.receiptid,
                prv_errwarn          => 'W',
                prv_comments         => 'ASETXN Partial Acknowledgement - EventCodeDesc:' || lv_wm_asetxnpart_rec.eventcodedesc || ' EventContext:' || lv_wm_asetxnpart_rec.eventcontext);
            ELSE
              pr_txn_lifecycle_rec(
                prv_errwarn  => 'W',
                prv_comments => 'Unable to find ACK or NACK for ASETXN');
            END IF;
            CLOSE lv_wm_asetxnpart_cur;
          END IF;
          CLOSE lv_wm_asetxnnack_cur;
        END IF;
        CLOSE lv_wm_asetxnack_cur;
      ELSE 
        pr_txn_lifecycle_rec(
          prv_errwarn  => 'W',
          prv_comments => 'ASETXN - Not found');
      END IF;
      CLOSE lv_wm_asetxn_cur;            
      ---- End ASETXN processing ----
    ELSE 
      pr_txn_lifecycle_rec(
        prv_errwarn  => 'W',
        prv_comments => 'EBTXN - Not found');
    END IF;
    CLOSE lv_wm_ebtxn_cur;
    ---- End EBTXN processing ----
  END pr_eb_ase_txn;

  ------------------------------------------------------------------------------------------------------------------
  -- Create lifecycle history of the specified transaction for use by the calling report
  ------------------------------------------------------------------------------------------------------------------
  FUNCTION fn_txn_lifecycle
  RETURN ty_txn_lifecycle PIPELINED
  IS
    lv_populated_params INTEGER := 0;
    TYPE ty_cdxwfrectype IS RECORD
    (
      wf_proc_id         VARCHAR2(10),
      wf_type            VARCHAR2(30),
      service_order      VARCHAR2(30),
      originating_txn    VARCHAR2(100),
      job_enquiry_code   VARCHAR2(60),
      field_activity     VARCHAR2(30),
      wf_creation_date   DATE,
      wf_status_reason   VARCHAR2(60)
    );
    TYPE ty_cdxfarectype IS RECORD
    (
      fa_id              VARCHAR2(10),
      fa_type            VARCHAR2(30),
      fa_status          VARCHAR2(30),
      fa_stage_status    VARCHAR2(2),
      cancel_reason      VARCHAR2(30),
      creation_date      DATE,
      schedule_date      DATE
    );
    TYPE ty_aserectype IS RECORD
    (
      docid              VARCHAR2(64),
      messageid          VARCHAR2(64),
      transactionid      VARCHAR2(64)
    );
    TYPE ty_mxosrrectype IS RECORD
    (
      srnum              VARCHAR2(10),
      status             VARCHAR2(10),
      statusdate         DATE,
      creationdate       DATE,
      cdx_field_activity VARCHAR2(10),
      retailer_reference VARCHAR2(30),
      related_wonum      VARCHAR2(10)
    );
    TYPE ty_mxoworectype IS RECORD
    (
      wonum              VARCHAR2(10),
      status             VARCHAR2(10),
      statusdate         DATE,
      creationdate       DATE,
      targstartdate      DATE,
      actstart           DATE,
      actfinish          DATE,
      related_srnum      VARCHAR2(10)
    );
    TYPE ty_gmnaemorectype IS RECORD
    (
      messageidpos       INTEGER,
      messageid          VARCHAR2(64),
      messagedatepos     INTEGER,
      messagedate        VARCHAR2(20)
    );
    
    lv_originating_txn   VARCHAR2(50);
    lv_service_order     VARCHAR2(50);
    lv_cdx_wf_rec        ty_cdxwfrectype;
    lv_cdx_fa_rec        ty_cdxfarectype;
    lv_ase_rec           ty_aserectype;
    lv_mxo_sr_rec        ty_mxosrrectype;
    lv_mxo_wo_rec        ty_mxoworectype;
    lv_gmnaemo_rec       ty_gmnaemorectype;
    lv_cdx_wf_cursor     ty_refcur;
    lv_cdx_fa_cursor     ty_refcur;
    lv_ase_cursor        ty_refcur;
    lv_mxo_sr_cursor     ty_refcur;
    lv_mxo_wo_cursor     ty_refcur;
    lv_gmnaemo_cur       ty_refcur;
    lv_wm_sordnew_cur    ty_refcur;
    lv_wm_sordnew_rec    sordrequestnew%ROWTYPE;
    lv_wm_sordinit_cur   ty_refcur;
    lv_wm_sordinit_rec   sordresponseinitial%ROWTYPE;
    lv_wm_sordclo_cur    ty_refcur;
    lv_wm_sordclo_rec    sordresponseclosure%ROWTYPE;
    lv_wm_sordcan_cur    ty_refcur;
    lv_wm_sordcan_rec    sordrequestcancel%ROWTYPE;
    
    -- Use a dynamic SQL statement to retrieve related ids from CCB
    -- Based on the identifier provided an appropriate join condition is added to the main statement 
    lv_cdx_wf_sql        VARCHAR2(4000) := '
      SELECT wf.wf_proc_id,
             RTRIM(wf.nt_up_xtype_cd) AS wf_type,
             (
               SELECT RTRIM(wfctxt.ctxt_val)
               FROM   cisadm.ci_wf_proc_ctxt@cdx_link wfctxt
               WHERE  wfctxt.wf_proc_id = wf.wf_proc_id
               AND    wfctxt.wf_proc_ctxt_flg = ''SONB''
             ) AS service_order, 
             (
               SELECT RTRIM(wfctxt.ctxt_val)
               FROM   cisadm.ci_wf_proc_ctxt@cdx_link wfctxt
               WHERE  wfctxt.wf_proc_id = wf.wf_proc_id
               AND    wfctxt.wf_proc_ctxt_flg = ''TRID''
             ) AS originating_txn,
             (
               SELECT fat.descr
               FROM   cisadm.ci_wf_proc_ctxt@cdx_link wfctxt
               JOIN   cisadm.ci_fa_type_l@cdx_link fat
                  ON  fat.fa_type_cd = wfctxt.ctxt_val
               WHERE  wfctxt.wf_proc_id = wf.wf_proc_id
               AND    wfctxt.wf_proc_ctxt_flg = ''JECD''
             ) AS job_enquiry_code,
             (
               SELECT RTRIM(wfctxt.ctxt_val)
               FROM   cisadm.ci_wf_proc_ctxt@cdx_link wfctxt
               WHERE  wfctxt.wf_proc_id = wf.wf_proc_id
               AND    wfctxt.wf_proc_ctxt_flg = ''SPRF''
             ) AS field_activity,
             wf.cre_dttm AS wf_creation_date,
             lok.descr AS wf_status_reason
      FROM   cisadm.ci_wf_proc@cdx_link wf
      JOIN   cisadm.ci_lookup@cdx_link lok
         ON  lok.field_name = ''WF_STAT_RSN_FLG''
         AND wf.wf_stat_rsn_flg = lok.field_value
      JOIN   cisadm.ci_wf_proc_ctxt@cdx_link wfatty
         ON  wfatty.wf_proc_id = wf.wf_proc_id
         AND wfatty.wf_proc_ctxt_flg = ''ATTY''
         AND COALESCE(wfatty.ctxt_val,wfatty.ctxt_val) = :action_type
    ';
    lv_cdx_wf_row_sql    VARCHAR2(1000);
    lv_cdx_fa_sql        VARCHAR2(1000) := '
      SELECT fa.fa_id,
             typ.descr AS fa_type,
             lok.descr AS fa_status,
             fa_up_status_flg AS fa_stage_status,
             can.descr AS cancel_reason,
             fa.cre_dttm AS creation_date,
             fa.sched_dttm AS schedule_date
      FROM   cisadm.ci_fa@cdx_link fa
      LEFT OUTER JOIN cisadm.ci_fa_type_l@cdx_link typ
         ON  fa.fa_type_cd = typ.fa_type_cd
      LEFT OUTER JOIN cisadm.ci_fa_can_rsn_l@cdx_link can
         ON  fa.fa_can_rsn_cd = can.fa_can_rsn_cd
      LEFT OUTER JOIN cisadm.ci_fa_stage_up@cdx_link fsu
         ON  fsu.fa_id = fa.fa_id
      JOIN   cisadm.ci_lookup@cdx_link lok
         ON  lok.field_name = ''FA_STATUS_FLG''
         AND lok.field_value = fa.fa_status_flg
      WHERE  fa.fa_id = :field_activity
    ';
    lv_ase_sql           VARCHAR2(1000) := '
      SELECT acc.docid,
             acc.messageid,
             acc.initiatingtransactionid AS transactionid
      FROM   asetxnackaccept acc
      WHERE  acc.initiatingtransactionid = :initiatingtransactionid
      UNION
      SELECT par.docid,
             par.messageid,
             par.initiatingtransactionid AS transactionid
      FROM   asetxnackpartial par
      WHERE  par.initiatingtransactionid = :initiatingtransactionid
      UNION
      SELECT rej.docid,
             rej.messageid,
             rej.initiatingtransactionid AS transactionid
      FROM   asetxnackreject rej
      WHERE  rej.initiatingtransactionid = :initiatingtransactionid
    ';
    lv_mxo_sr_sql        VARCHAR2(4000) := '
      SELECT tkt.ticketid AS srnum,
             tkt.status,
             tkt.statusdate,
             (
               SELECT MIN(changedate)
               FROM   maximo.tkstatus@mx7_link tks
               WHERE  tks.siteid = tkt.siteid
               AND    tks.ticketid = tkt.ticketid
             ) AS creationdate,
             (
               SELECT tsp.alnvalue
               FROM   maximo.ticketspec@mx7_link tsp
               WHERE  tsp.siteid = tkt.siteid
               AND    tsp.ticketid = tkt.ticketid
               AND    tsp.assetattrid = ''CDX''
             ) AS cdx_field_activity,  
             (
               SELECT tsp.alnvalue
               FROM   maximo.ticketspec@mx7_link tsp
               WHERE  tsp.siteid = tkt.siteid
               AND    tsp.ticketid = tkt.ticketid
               AND    tsp.assetattrid = ''RET_REF_NUM''
             ) AS retailer_reference,
             (
               SELECT MIN(relatedreckey)
               FROM   maximo.relatedrecord@mx7_link rr
               WHERE  rr.siteid = tkt.siteid
               AND    rr.recordkey = tkt.ticketid
               AND    rr.class = ''SR''
               AND    rr.relatedrecclass = ''WORKORDER''
               AND    rr.relatetype = ''FOLLOWUP''
             ) AS related_wonum

      FROM   maximo.ticket@mx7_link tkt
      JOIN   maximo.ticketspec@mx7_link tsp
         ON  tsp.siteid = tkt.siteid
         AND tsp.ticketid = tkt.ticketid
         AND tsp.assetattrid = ''CDX''
    ';
    lv_mxo_sr_row_sql    VARCHAR2(100);

    lv_mxo_wo_sql        VARCHAR2(4000) := '
      SELECT wo.wonum,
             wo.status,
             wo.statusdate,
             (
               SELECT MIN(changedate)
               FROM   maximo.wostatus@mx7_link wos
               WHERE  wos.siteid = wo.siteid
               AND    wos.wonum = wo.wonum
             ) AS creationdate,
             wo.targstartdate,
             wo.actstart,
             wo.actfinish,
             (
               SELECT MIN(relatedreckey)
               FROM   maximo.relatedrecord@mx7_link rr
               WHERE  rr.siteid = wo.siteid
               AND    rr.recordkey = wo.wonum
               AND    rr.class = ''WORKORDER''
               AND    rr.relatedrecclass = ''SR''
               AND    rr.relatetype = ''ORIGINATOR''
             ) AS related_srnum
      FROM   maximo.workorder@mx7_link wo
      WHERE  wo.wonum = :work_order
    ';

    lv_gmnaemo_sql       VARCHAR2(4000) := '
      SELECT INSTR(TO_CHAR(xai.xml_request),''<MessageID'') AS messageidpos,
             SUBSTR(TO_CHAR(xai.xml_request),INSTR(TO_CHAR(xai.xml_request),''<MessageID'')+11,INSTR(TO_CHAR(xai.xml_request),''</MessageID'')-INSTR(TO_CHAR(xai.xml_request),''<MessageID'')-11) AS messageid,
             INSTR(TO_CHAR(xai.xml_request),''<MessageDate'') AS gmndatepos,
             REPLACE(SUBSTR(TO_CHAR(xai.xml_request),INSTR(TO_CHAR(xai.xml_request),''<MessageDate'')+13,19),''T'','' '') AS gmndate
      FROM   cisadm.ci_wf_evt_ctxt@cdx_link wctx
      JOIN   cisadm.ci_xai_dwn@cdx_link xai
         ON  xai.nt_dwn_id = wctx.ctxt_val
         AND xai.xai_rt_type_cd LIKE ''%GASMTRNO''
      WHERE  wctx.wf_proc_id = :cdxwfprocid
      AND    wctx.wf_evt_ctxt_flg = ''NDS''
    ';

  BEGIN
    -- Confirm that only one parameter has been populated by the user
    IF pkv_originating_txn IS NOT NULL THEN
      lv_populated_params := lv_populated_params + 1;
    END IF;
    IF pkv_service_order IS NOT NULL THEN
      lv_populated_params := lv_populated_params + 1;
    END IF; 
    IF pkv_field_activity IS NOT NULL THEN
      lv_populated_params := lv_populated_params + 1;
    END IF; 
    IF pkv_service_request IS NOT NULL THEN
      lv_populated_params := lv_populated_params + 1;
    END IF; 
    IF pkv_work_order IS NOT NULL THEN
      lv_populated_params := lv_populated_params + 1;
    END IF; 
    IF lv_populated_params = 0 THEN
      pr_txn_lifecycle_rec(
        prv_errwarn      => 'E',
        prv_comments     => 'A value must be entered in one - and only one - of the parameters');
    ELSIF lv_populated_params > 1 THEN
      pr_txn_lifecycle_rec(
        prv_errwarn      => 'E',
        prv_comments     => 'A value must be entered in only one of the parameters');
    ELSE

      -- Attempt to find Maximo work order if identifier has been provided
      IF pkv_work_order IS NOT NULL THEN
        OPEN lv_mxo_wo_cursor FOR lv_mxo_wo_sql USING pkv_work_order;
        FETCH lv_mxo_wo_cursor INTO lv_mxo_wo_rec;
        -- Assign service request related to work order
        IF lv_mxo_wo_cursor%FOUND THEN
          pkv_service_request := lv_mxo_wo_rec.related_srnum;
        END IF;
        CLOSE lv_mxo_wo_cursor;
      END IF; 

      -- Attempt to find Maximo service request if identifier has been provided
      IF pkv_service_request IS NOT NULL THEN
        lv_mxo_sr_row_sql := '
          WHERE  tkt.ticketid = ''' || pkv_service_request || '''';
        OPEN lv_mxo_sr_cursor FOR lv_mxo_sr_sql||lv_mxo_sr_row_sql;
        FETCH lv_mxo_sr_cursor INTO lv_mxo_sr_rec;
        -- Assign CDX field activity referenced by service request
        IF lv_mxo_sr_cursor%FOUND THEN
          pkv_field_activity := lv_mxo_sr_rec.cdx_field_activity;
          pkv_work_order := lv_mxo_sr_rec.related_wonum;
        END IF;
        CLOSE lv_mxo_sr_cursor;
      END IF; 

      -- Add the appropriate CCB join condition to find the record based on the identifier provided
      IF pkv_originating_txn IS NOT NULL THEN
        lv_cdx_wf_row_sql := '
          JOIN   cisadm.ci_wf_proc_ctxt@cdx_link wftrid
             ON  wftrid.wf_proc_id = wf.wf_proc_id
             AND wftrid.wf_proc_ctxt_flg = ''TRID''
             AND wftrid.ctxt_val = ''' || pkv_originating_txn || '''';
      END IF;
      IF pkv_service_order IS NOT NULL THEN
        lv_cdx_wf_row_sql := '
          JOIN   cisadm.ci_wf_proc_ctxt@cdx_link wfsonb
             ON  wfsonb.wf_proc_id = wf.wf_proc_id
             AND wfsonb.wf_proc_ctxt_flg = ''SONB''
             AND wfsonb.ctxt_val = ''' || pkv_service_order || '''';
      END IF; 
      IF pkv_field_activity IS NOT NULL THEN
        lv_cdx_wf_row_sql := '
          JOIN cisadm.ci_wf_proc_ctxt@cdx_link wfsprf
             ON  wfsprf.wf_proc_id = wf.wf_proc_id 
             AND wfsprf.wf_proc_ctxt_flg = ''SPRF''
             AND wfsprf.ctxt_val = ''' || pkv_field_activity || '''';
      END IF; 
      
      IF pkv_originating_txn IS NOT NULL OR pkv_service_order IS NOT NULL OR pkv_field_activity IS NOT NULL THEN
        -- Attempt to find CCB details for the identifier provided
        OPEN lv_cdx_wf_cursor FOR lv_cdx_wf_sql||lv_cdx_wf_row_sql USING 'New';
        FETCH lv_cdx_wf_cursor INTO lv_cdx_wf_rec;

        -- Attempt to retrieve related ids from Web Methods for the transaction id/service order provided or retrieved from CCB
        lv_service_order := COALESCE(lv_cdx_wf_rec.service_order,pkv_service_order);
        lv_originating_txn := COALESCE(lv_cdx_wf_rec.originating_txn,pkv_originating_txn);
        -- Query using service order number if available as this is more reliably populated in table
        IF lv_service_order IS NOT NULL THEN
          OPEN lv_wm_sordnew_cur FOR 'SELECT * FROM sordrequestnew WHERE  serviceorderno = :serviceorder' USING lv_service_order;
        ELSE
          OPEN lv_wm_sordnew_cur FOR 'SELECT * FROM sordrequestnew WHERE  transactionid = :transactionid' USING lv_originating_txn;
        END IF;
        FETCH lv_wm_sordnew_cur INTO lv_wm_sordnew_rec;
        IF lv_wm_sordnew_cur%FOUND THEN
          pr_txn_lifecycle_rec(
            prv_wm_docid         => lv_wm_sordnew_rec.docid,
            prv_wm_messageid     => lv_wm_sordnew_rec.messageid,
            prv_wm_transactionid => lv_wm_sordnew_rec.transactionid,
            prv_wm_service_order => lv_wm_sordnew_rec.serviceorderno,
            prv_comments         => 'New Service Order Request - Address: ' || lv_wm_sordnew_rec.housenumber || ' ' || lv_wm_sordnew_rec.streetname || ' ' || lv_wm_sordnew_rec.streettype || ' ' ||
              lv_wm_sordnew_rec.suburborplaceorlocality || ' ' || lv_wm_sordnew_rec.stateorterritory || ' ' || lv_wm_sordnew_rec.postcode || ' JobEnquiryCode: ' || lv_wm_sordnew_rec.jobenquirycode ||
              ' AppointmentPreferredDate: ' || TO_CHAR(lv_wm_sordnew_rec.appointmentpreferreddate,'DD/MM/YYYY'));

          -- Retrieve EBTXN and ASETXN details associated with the new service order
          pr_eb_ase_txn(prv_wm_docid         => lv_wm_sordnew_rec.docid,
                        prv_wm_messageid     => lv_wm_sordnew_rec.messageid,
                        prv_wm_transactionid => lv_wm_sordnew_rec.transactionid,
                        prv_wm_service_order => lv_wm_sordnew_rec.serviceorderno);

          -- Find initial response to retailer
          OPEN lv_wm_sordinit_cur FOR 'SELECT * FROM sordresponseinitial WHERE initiatingtransactionid = :transactionid' USING lv_wm_sordnew_rec.transactionid;
          FETCH lv_wm_sordinit_cur INTO lv_wm_sordinit_rec;
          IF lv_wm_sordinit_cur%FOUND THEN
            pr_txn_lifecycle_rec(
              prv_txn_date         => lv_wm_sordinit_rec.transactiondate,
              prv_wm_docid         => lv_wm_sordinit_rec.docid,
              prv_wm_messageid     => lv_wm_sordinit_rec.messageid,
              prv_wm_transactionid => lv_wm_sordinit_rec.transactionid,
              prv_wm_service_order => lv_wm_sordinit_rec.serviceorderno,
              prv_comments         => 'Initial Response to Service Order - JobEnquiryCode:' || lv_wm_sordinit_rec.jobenquirycode || ' ApptPreferredDate:' || TO_CHAR(lv_wm_sordinit_rec.apptpreferreddate,'DD/MM/YYYY') || ' MIRN:' || lv_wm_sordinit_rec.mirn);
            -- Retrieve EBTXN and ASETXN details associated with the initial response
            pr_eb_ase_txn(prv_wm_docid         => lv_wm_sordinit_rec.docid,
                          prv_wm_messageid     => lv_wm_sordinit_rec.messageid,
                          prv_wm_transactionid => lv_wm_sordinit_rec.transactionid,
                          prv_wm_service_order => lv_wm_sordinit_rec.serviceorderno);
          END IF;
          CLOSE lv_wm_sordinit_cur;
        ELSE
          -- If we couldn't find Service Order Request and user gave us a transaction id then try to find an ASE accept/reject/partial using it
          IF lv_originating_txn IS NOT NULL THEN
            OPEN lv_ase_cursor FOR lv_ase_sql USING lv_originating_txn, lv_originating_txn, lv_originating_txn;
            FETCH lv_ase_cursor INTO lv_ase_rec;
            IF lv_ase_cursor%FOUND THEN
              -- Retrieve EBTXN and ASETXN details associated with the docid/transactionid
              pr_eb_ase_txn(prv_wm_docid         => lv_ase_rec.docid,
                            prv_wm_messageid     => lv_ase_rec.messageid,
                            prv_wm_transactionid => lv_ase_rec.transactionid);
            ELSE                
              pr_txn_lifecycle_rec(
              prv_errwarn => 'W',
              prv_comments => 'Unable to find any Web Methods details for the identifier provided');
            END IF;
            CLOSE lv_ase_cursor;
          ELSE
            pr_txn_lifecycle_rec(
              prv_errwarn  => 'W',
              prv_comments => 'Unable to find Service Order Request for the identifier provided');
          END IF;
        END IF;
        CLOSE lv_wm_sordnew_cur;

        -- Display details of CCB workflow queried above
        IF lv_cdx_wf_cursor%FOUND THEN
          pr_txn_lifecycle_rec(
            prv_txn_date         => lv_cdx_wf_rec.wf_creation_date,
            prv_wm_transactionid => lv_cdx_wf_rec.originating_txn,
            prv_wm_service_order => lv_cdx_wf_rec.service_order,
            prv_cdx_wf_proc_id   => lv_cdx_wf_rec.wf_proc_id,
            prv_comments         => 'CCB Workflow for Service Order created - JobEnquiryDesc:' || lv_cdx_wf_rec.job_enquiry_code || ' StatusReason:' || lv_cdx_wf_rec.wf_status_reason );

          -- Display details of associated field activity
          IF lv_cdx_wf_rec.field_activity IS NOT NULL THEN
            OPEN lv_cdx_fa_cursor FOR lv_cdx_fa_sql USING lv_cdx_wf_rec.field_activity;
            FETCH lv_cdx_fa_cursor INTO lv_cdx_fa_rec;
            IF lv_cdx_fa_cursor%FOUND THEN
              pkv_field_activity := lv_cdx_wf_rec.field_activity;
              pr_txn_lifecycle_rec(
                prv_txn_date    => lv_cdx_fa_rec.creation_date,
                prv_cdx_fa_id   => lv_cdx_fa_rec.fa_id,
                prv_comments    => 'CCB Field Activity created - Type:' || lv_cdx_fa_rec.fa_type || ' Status:' || lv_cdx_fa_rec.fa_status || ' Staging Status:' || lv_cdx_fa_rec.fa_stage_status || ' ScheduleDate:' || TO_CHAR(lv_cdx_fa_rec.schedule_date,'DD/MM/YYYY') || ' CancelReason:' || lv_cdx_fa_rec.cancel_reason);
            ELSE
              pr_txn_lifecycle_rec(
                prv_errwarn  => 'W',
                prv_comments => 'Unable to find CCB field activity for workflow');
            END IF;
            CLOSE lv_cdx_fa_cursor;
          END IF;
        ELSE
          pr_txn_lifecycle_rec(
            prv_errwarn => 'W',
            prv_comments => 'Unable to find CCB Workflow for Service Order creation');
        END IF;
        CLOSE lv_cdx_wf_cursor;
      ELSE
        pr_txn_lifecycle_rec(
          prv_errwarn => 'W',
          prv_comments => 'No CCB Workflow associated with Maximo Work Order/Service Request');
      END IF;

      -- If Maximo service request hasn't already been queried, attempt to find it using CCB field activity
      IF lv_mxo_sr_rec.srnum IS NULL THEN
        IF pkv_field_activity IS NOT NULL THEN
          lv_mxo_sr_row_sql := '
            WHERE  tsp.alnvalue = ''' || pkv_field_activity || '''';
          OPEN lv_mxo_sr_cursor FOR lv_mxo_sr_sql||lv_mxo_sr_row_sql;
          FETCH lv_mxo_sr_cursor INTO lv_mxo_sr_rec;
          IF lv_mxo_sr_cursor%NOTFOUND THEN 
            pr_txn_lifecycle_rec(
              prv_errwarn  => 'W',
              prv_comments => 'Unable to find MXO service request for CCB field activity');
          ELSE
            pkv_work_order := lv_mxo_sr_rec.related_wonum;
          END IF;
          CLOSE lv_mxo_sr_cursor;
        END IF;
      END IF;  
      -- Display details of service request if found
      IF lv_mxo_sr_rec.srnum IS NOT NULL THEN
        pr_txn_lifecycle_rec(
          prv_txn_date         => lv_mxo_sr_rec.creationdate,
          prv_cdx_fa_id        => lv_mxo_sr_rec.cdx_field_activity,
          prv_mxo_srnum        => lv_mxo_sr_rec.srnum,
          prv_mxo_wonum        => lv_mxo_sr_rec.related_wonum,
          prv_comments         => 'MXO Service Request created - Status:' || lv_mxo_sr_rec.status || ' StatusDate:' || TO_CHAR(lv_mxo_sr_rec.statusdate,'DD/MM/YYYY'));
      END IF;

      -- If Maximo work order hasn't already been queried, attempt to find it using service request
      IF lv_mxo_wo_rec.wonum IS NULL THEN
        IF pkv_work_order IS NOT NULL THEN
          OPEN lv_mxo_wo_cursor FOR lv_mxo_wo_sql USING pkv_work_order;
          FETCH lv_mxo_wo_cursor INTO lv_mxo_wo_rec;
          IF lv_mxo_wo_cursor%NOTFOUND THEN 
            pr_txn_lifecycle_rec(
              prv_mxo_wonum => pkv_work_order,
              prv_errwarn   => 'E',              
              prv_comments  => 'Unable to find MXO work order');
          END IF;
          CLOSE lv_mxo_wo_cursor;
        END IF;
      END IF;  
      -- Display details of work order if found
      IF lv_mxo_wo_rec.wonum IS NOT NULL THEN
        pr_txn_lifecycle_rec(
          prv_txn_date         => lv_mxo_wo_rec.creationdate,
          prv_mxo_wonum        => lv_mxo_wo_rec.wonum,
          prv_comments         => 'MXO Work Order created - Status:' || lv_mxo_wo_rec.status || ' StatusDate:' || TO_CHAR(lv_mxo_wo_rec.statusdate,'DD/MM/YYYY') || 
                                  ' TargetStart:' || TO_CHAR(lv_mxo_wo_rec.targstartdate,'DD/MM/YYYY') || ' ActualStart:' || TO_CHAR(lv_mxo_wo_rec.actstart,'DD/MM/YYYY') ||
                                  ' ActualFinish:' || TO_CHAR(lv_mxo_wo_rec.actfinish,'DD/MM/YYYY'));
      END IF;

      -- Check for cancellation of the service order
      -- Query using service order number if available as this is more reliably populated in table
      IF lv_service_order IS NOT NULL THEN
        OPEN lv_wm_sordcan_cur FOR 'SELECT * FROM sordrequestcancel WHERE  serviceorderno = :serviceorder' USING lv_service_order;
      ELSE
        OPEN lv_wm_sordcan_cur FOR 'SELECT * FROM sordrequestcancel WHERE  initiatingtransactionid = :transactionid' USING lv_originating_txn;
      END IF;
      FETCH lv_wm_sordcan_cur INTO lv_wm_sordcan_rec;
      IF lv_wm_sordcan_cur%FOUND THEN
        pr_txn_lifecycle_rec(
          prv_txn_date         => lv_wm_sordcan_rec.transactiondate,
          prv_wm_docid         => lv_wm_sordcan_rec.docid,
          prv_wm_messageid     => lv_wm_sordcan_rec.messageid,
          prv_wm_transactionid => lv_wm_sordcan_rec.transactionid,
          prv_wm_service_order => lv_wm_sordcan_rec.serviceorderno,
          prv_comments     => 'Cancellation of Service Order - JobEnquiryCode:' || lv_wm_sordcan_rec.jobenquirycode || ' MIRN:' || lv_wm_sordcan_rec.mirn);

        -- Retrieve EBTXN and ASETXN details associated with the cancellation request
        pr_eb_ase_txn(prv_wm_docid         => lv_wm_sordcan_rec.docid,
                      prv_wm_messageid     => lv_wm_sordcan_rec.messageid,
                      prv_wm_transactionid => lv_wm_sordcan_rec.transactionid,
                      prv_wm_service_order => lv_wm_sordcan_rec.serviceorderno);
       
        -- Attempt to find CCB details for SO cancellation
        lv_cdx_wf_row_sql := '
        JOIN   cisadm.ci_wf_proc_ctxt@cdx_link wfsonb
           ON  wfsonb.wf_proc_id = wf.wf_proc_id
           AND wfsonb.wf_proc_ctxt_flg = ''SONB''
           AND wfsonb.ctxt_val = ''' || lv_service_order || '''';
        OPEN lv_cdx_wf_cursor FOR lv_cdx_wf_sql||lv_cdx_wf_row_sql USING 'Cancel';
        FETCH lv_cdx_wf_cursor INTO lv_cdx_wf_rec;
        IF lv_cdx_wf_cursor%FOUND THEN
          pr_txn_lifecycle_rec(
            prv_txn_date         => lv_cdx_wf_rec.wf_creation_date,
            prv_wm_transactionid => lv_cdx_wf_rec.originating_txn,
            prv_wm_service_order => lv_cdx_wf_rec.service_order,
            prv_cdx_wf_proc_id   => lv_cdx_wf_rec.wf_proc_id,
            prv_comments     => 'CCB Workflow for Service Order cancellation - JobEnquiryDesc:' || lv_cdx_wf_rec.job_enquiry_code || ' StatusReason:' || lv_cdx_wf_rec.wf_status_reason );
        ELSE
          pr_txn_lifecycle_rec(
            prv_errwarn => 'W',
            prv_comments => 'Unable to find CCB Workflow for Service Order cancellation');
        END IF;
      CLOSE lv_cdx_wf_cursor;

      END IF;
      CLOSE lv_wm_sordcan_cur;

      -- Check for closure of the service order
      -- Query using service order number if available as this is more reliably populated in table
      IF lv_service_order IS NOT NULL THEN
        OPEN lv_wm_sordclo_cur FOR 'SELECT * FROM sordresponseclosure WHERE  serviceorderno = :serviceorder' USING lv_service_order;
      ELSE
        OPEN lv_wm_sordclo_cur FOR 'SELECT * FROM sordresponseclosure WHERE  initiatingtransactionid = :transactionid' USING lv_originating_txn;
      END IF;
      FETCH lv_wm_sordclo_cur INTO lv_wm_sordclo_rec;
      IF lv_wm_sordclo_cur%FOUND THEN
        pr_txn_lifecycle_rec(
          prv_txn_date         => lv_wm_sordclo_rec.transactiondate,
          prv_wm_docid         => lv_wm_sordclo_rec.docid,
          prv_wm_messageid     => lv_wm_sordclo_rec.messageid,
          prv_wm_transactionid => lv_wm_sordclo_rec.transactionid,
          prv_wm_service_order => lv_wm_sordclo_rec.serviceorderno,
          prv_comments     => 'Closure of Service Order - OrderCompletedDate:' || TO_CHAR(lv_wm_sordclo_rec.ordercompleteddate,'DD/MM/YYYY') || 'JobEnquiryCode:' || lv_wm_sordclo_rec.jobenquirycode || ' MIRN:' || lv_wm_sordclo_rec.mirn);

        -- Retrieve EBTXN and ASETXN details associated with the closure response
        pr_eb_ase_txn(prv_wm_docid         => lv_wm_sordclo_rec.docid,
                      prv_wm_messageid     => lv_wm_sordclo_rec.messageid,
                      prv_wm_transactionid => lv_wm_sordclo_rec.transactionid,
                      prv_wm_service_order => lv_wm_sordclo_rec.serviceorderno);
      END IF;
      CLOSE lv_wm_sordclo_cur;

      -- Check for Gas Meter Notification to AEMO
      IF lv_cdx_wf_rec.wf_proc_id IS NOT NULL THEN
        OPEN lv_gmnaemo_cur FOR lv_gmnaemo_sql USING lv_cdx_wf_rec.wf_proc_id;
        FETCH lv_gmnaemo_cur INTO lv_gmnaemo_rec;
        IF lv_gmnaemo_cur%FOUND AND lv_gmnaemo_rec.messageidpos > 0 THEN
          pr_txn_lifecycle_rec(
            prv_txn_date         => TO_DATE(lv_gmnaemo_rec.messagedate,'YYYY-MM-DD HH24:MI:SS'),
            prv_wm_messageid     => lv_gmnaemo_rec.messageid,
            prv_comments     => 'Gas Meter Notification to AEMO');
        END IF;
        CLOSE lv_gmnaemo_cur;
      END IF;
    END IF;

    -- Pipe out all records that have been populated
    FOR i IN pkv_eb_asn_txn.FIRST..pkv_eb_asn_txn.LAST 
    LOOP
      PIPE ROW(pkv_eb_asn_txn(i));
    END LOOP;

  END fn_txn_lifecycle;

END tm_txn_lifecycle;
/

SHOW ERRORS
/

CREATE OR REPLACE VIEW tm.tm_txn_lifecycle_vw AS
  SELECT row_sequence,
         txn_date,
         wm_docid,
         wm_messageid,
         wm_transactionid,
         wm_service_order,
         cdx_wf_proc_id,
         cdx_fa_id,
         mxo_srnum,
         mxo_wonum,
         errwarn,
         comments
  FROM   TABLE(tm.tm_txn_lifecycle.fn_txn_lifecycle)
/

GRANT EXECUTE ON tm.tm_txn_lifecycle TO readonly
/

GRANT SELECT ON tm.tm_txn_lifecycle_vw TO readonly
/
