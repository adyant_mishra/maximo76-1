/*
 * This file contains functions, for use across all reports
 * 
 */


// Return the maximo user
function getUserName() {
  var userName = reportContext.getHttpServletRequest().getParameter("userName");
  
  if (userName == null || userName == "") {
    var reportJobId = mxReportScriptContext.getMXReportContext().get("REPORTJOBID");
    if (reportJobId != null) {
      var reportJobDS = MXReportDataSetProvider.create("maximoDataSource", "reportJobDS");
      reportJobDS.open();
      var sql = "SELECT USERID FROM REPORTJOB WHERE REPORTJOBID = ?";
      reportJobDS.setQuery(sql);
      reportJobDS.setQueryParameterValue(1, reportJobId);
      
      if(reportJobDS.fetch())
        userName = reportJobDS.getString("USERID");
      
      reportJobDS.close();
    }
  }

  return userName;
}


//Returns the site name
function getMroSite() {
  var mroSite = reportContext.getHttpServletRequest().getParameter("mroSite");
  return mroSite;
}


// Returns the organisation name
function getMroOrg() {
  var mroOrg = reportContext.getHttpServletRequest().getParameter("mroOrg");
  return mroOrg;
}


// Returns the report design filename
function getReportFileName() {
  var reportFile;
  if  (mxReportScriptContext.getMXReportContext().get("REPORTRUNINFOKEY") != null && mxReportScriptContext.getMXReportContext().get("REPORTRUNINFOKEY") != "") { 
    reportFile = mxReportScriptContext.getMXReportContext().get("REPORTRUNINFOKEY").substring(0,mxReportScriptContext.getMXReportContext().get("REPORTRUNINFOKEY").indexOf("rptdesign")+9);
  } else {
    // This is a dirty way to differentiate between a HSE vs EAM report - Used for Direct Print where REPORTRUNINFOKEY isn't set
    // EAM reports have been created from a template that contains a table called outerReportTable
    // HSE reports don't have this
    if (reportContext.getDesignHandle().findElement("outerReportTable") == null)
      reportFile = "apahse_Unknown.rptdesign";
    else  
      reportFile = "APA_Unknown.rptdesign";
  }
  
  return reportFile;
};
