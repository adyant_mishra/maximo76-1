#!/bin/sh


REPORT_TOOL_DIR=`dirname $0`
SCRIPT_DIR=$REPORT_TOOL_DIR/../../../tools/maximo
SCRIPT_NAME=writereportprop.xml
MAXIMO_SCRIPT_CP=$SCRIPT_DIR/internal

if [ "$MAXIMO_JAVA" = "" ] ; then
	MAXIMO_JAVA=$SCRIPT_DIR/../java/jre/bin/java
fi

"$MAXIMO_JAVA" -cp "$MAXIMO_SCRIPT_CP" MaximoScript "$SCRIPT_DIR/$SCRIPT_NAME" "$@"

exit $?
