@echo off

set JAVA_HOME=..\..\..\tools\java\jre

rem ------------- set Ant specific settings --------------
rem 
set ANT_HOME=..\..\..\tools\ant
set ANT_OPTS=%ANT_OPTS% -Xmx256m
rem 
rem ------------------------------------------------------

set MBOCLASSES=..\..\..\applications\maximo\businessobjects\classes

if "%1%"=="report" (
if "%2%"=="" (
goto usage
)
if "%3%"=="" (
goto usage
)

call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\exportreport.xml -Dmaximo.report.birt.appname=%2 -Dmaximo.report.birt.reportname=%3 %1  
goto end
)

if "%1%"=="library" (
if "%2%"=="" (
goto usage
)

call %ANT_HOME%\bin\ant -lib "%MBOCLASSES%" -buildfile .\exportreport.xml -Dmaximo.report.birt.reportname=%2 %1  
goto end
)


goto usage


:usage
echo Usage:
echo exportreport report [appname] [reportname]
echo      Exports a specific report of the specified app.
echo or
echo exportreport library [libraryname]
echo      Exports a specific library.
goto end

:end
@endlocal
