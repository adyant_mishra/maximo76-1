#!/bin/sh

showUsageMessage()
{
	echo 'Usage:'
	echo 'exportreport report [appname] [reportname]'
	echo      'Exports a specific report of the specified app.'
	echo 'or'
	echo 'exportreport library [libraryname]'
	echo      'Exports a specific library.'
}

export JAVA_HOME=../../../tools/java/jre

# ------------- export Ant specific settings --------------
# 
export ANT_HOME=../../../tools/ant
export ANT_OPTS="$ANT_OPTS -Xmx256m"
# 
# ------------------------------------------------------

export MBOCLASSES=../../../applications/maximo/businessobjects/classes

if [ "$1" = "report" ] ; then
	if [ -z "$2" ];then
		showUsageMessage
	else
		if [ -z "$3" ];then
			showUsageMessage
		else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./exportreport.xml -Dmaximo.report.birt.appname=$2 -Dmaximo.report.birt.reportname=$3 $1
		fi
	fi
elif [ "$1" = "library" ] ; then
	if [ -z "$2" ];then
		showUsageMessage
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./exportreport.xml -Dmaximo.report.birt.reportname=$2 $1  
	fi
else
	showUsageMessage
fi
