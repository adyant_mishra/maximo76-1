#!/bin/sh

showUsageMessage()
{
	echo 'Usage:'
	echo 'importreports'
	echo '  Imports all libraries and reports'
	echo 'or'
	echo 'importreports reports'
	echo '   Imports all reports'
	echo 'or'
	echo 'importreports report [appname] [report design name]'
	echo '   Imports single report'
	echo 'or'
	echo 'importreports app [appname]'
	echo '   Imports all reports for the given app'
	echo 'or'
	echo 'importreports libraries'
	echo '   Imports all libraries'
	echo 'or'
	echo 'importreports help'
	echo '   Displays this message'
}

export JAVA_HOME=../../../tools/java/jre

# ------------- export Ant specific settings --------------
# 
export ANT_HOME=../../../tools/ant
export ANT_OPTS="$ANT_OPTS -Xmx256m"
# 
# ------------------------------------------------------

export MBOCLASSES=../../../applications/maximo/businessobjects/classes

if [ -z "$1" ];then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./importreports.xml  
elif [ "$1" = "reports" ] ; then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./importreports.xml $1 
elif [ "$1" = "libraries" ] ; then
	$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./importreports.xml $1 
elif [ "$1" = "report" ] ; then
	if [ -z "$2" ] || [ -z "$3" ];then
		showUsageMessage
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./importreports.xml -Dmaximo.report.birt.appname="$2" -Dmaximo.report.birt.reportname="$3" "singlereport"
	fi
elif [ "$1" = "app" ] ; then
	if [ -z "$2" ];then
		showUsageMessage
	else
		$ANT_HOME/bin/ant -lib "$MBOCLASSES" -buildfile ./importreports.xml -Dmaximo.report.birt.appname=$2 $1 
	fi
else
	showUsageMessage
fi

