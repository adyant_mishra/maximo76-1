##########################################################
# Copyright IBM Corp. 2015
#
# APA HSE Project
#
# APAHSE_OBJ_PROBLEM_UPD
# Authors:	mgiang@au1.ibm.com
##########################################################
from psdi.mbo import MboConstants

print "In the APAHSE_OBJ_PROBLEM_UPD Script"

errorDisplayed = False
if app is not None and app =="APAHSEINV":
	# Validate the Asset Strike field, if it's checked at least what type should be checked
	if ( mbo.getBoolean("APAHSEASSETSTRIKE")):
		gas = mbo.getBoolean("APAHSESTRIKEGAS")
		electric = mbo.getBoolean("APAHSESTRIKEELEC")
		tele = mbo.getBoolean("APAHSESTRIKETEL")
		water  = mbo.getBoolean("APAHSESTRIKEWAT")
		sewer = mbo.getBoolean("APAHSESTRIKESEW")
		other = mbo.getBoolean("APAHSESTRIKEOTH")
		if (not gas and not electric and not water and not tele and not sewer and not other ):
			errorkey="AssetStrikeMandatory"
			errorgroup="apahseinc"
			errorDisplayed=True
		if not errorDisplayed and electric and gas:
			# Default to LEVEL 3 based on the potential injury to personnel
			# Check that the values have not changed, because if they have remained the same, someone might have overwridden the type on purpose
			previousElectric = mbo.getMboValue("APAHSESTRIKEELEC").getPreviousValue().asBoolean()
			previousGas = mbo.getMboValue("APAHSESTRIKEGAS").getPreviousValue().asBoolean()
			# Check that the values have changed
			if previousElectric is None or previousGas is None or not previousElectric or not previousGas:
				mbo.setValue("APAHSEINCRATING","LEVEL 3",MboConstants.NOACCESSCHECK)

	if not errorDisplayed:
		origrecordclass = mbo.getMboValue("origrecordclass").getString();
		incidentid = mbo.getMboValue("ORIGRECORDID").getString();
		if origrecordclass == "INCIDENT" and incidentid is not None:
			incidentSet = mbo.getMboSet("$incidentname","INCIDENT","TICKETID='" + incidentid + "'");
			incidentSet.reset();
			incidentmbo = incidentSet.getMbo(0);
			incRating = incidentmbo.getString("apahseincrating");
	
			if (incRating != mbo.getString("apahseincrating")):
				# No Action to avoid infinite looping
				incidentmbo.setValue("apahseincrating",mbo.getString("apahseincrating"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSEASSETSTRIKE") != mbo.getBoolean("APAHSEASSETSTRIKE")):
				incidentmbo.setValue("APAHSEASSETSTRIKE",mbo.getBoolean("APAHSEASSETSTRIKE"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSESTRIKEGAS") != mbo.getBoolean("APAHSESTRIKEGAS")):
				incidentmbo.setValue("APAHSESTRIKEGAS",mbo.getBoolean("APAHSESTRIKEGAS"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSESTRIKEELEC") != mbo.getBoolean("APAHSESTRIKEELEC")):
				incidentmbo.setValue("APAHSESTRIKEELEC",mbo.getBoolean("APAHSESTRIKEELEC"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSESTRIKETEL") != mbo.getBoolean("APAHSESTRIKETEL")):
				incidentmbo.setValue("APAHSESTRIKETEL",mbo.getBoolean("APAHSESTRIKETEL"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSESTRIKEWAT") != mbo.getBoolean("APAHSESTRIKEWAT")):
				incidentmbo.setValue("APAHSESTRIKEWAT",mbo.getBoolean("APAHSESTRIKEWAT"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSESTRIKESEW") != mbo.getBoolean("APAHSESTRIKESEW")):
				incidentmbo.setValue("APAHSESTRIKESEW",mbo.getBoolean("APAHSESTRIKESEW"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSESTRIKEOTH") != mbo.getBoolean("APAHSESTRIKEOTH")):
				incidentmbo.setValue("APAHSESTRIKEOTH",mbo.getBoolean("APAHSESTRIKEOTH"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSESTRIKEDETAIL") != mbo.getString("APAHSESTRIKEDETAIL")):
				incidentmbo.setValue("APAHSESTRIKEDETAIL",mbo.getString("APAHSESTRIKEDETAIL"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSEELECVOLT") != mbo.getString("APAHSEELECVOLT")):
				incidentmbo.setValue("APAHSEELECVOLT",mbo.getString("APAHSEELECVOLT"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSEGASPRESS") != mbo.getString("APAHSEGASPRESS")):
				incidentmbo.setValue("APAHSEGASPRESS",mbo.getString("APAHSEGASPRESS"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSEDEVICEUSED") != mbo.getString("APAHSEDEVICEUSED")):
				incidentmbo.setValue("APAHSEDEVICEUSED",mbo.getString("APAHSEDEVICEUSED"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				# Loss of Containment Values Update
			if (incidentmbo.getBoolean("APAHSELIQUID") != mbo.getBoolean("APAHSELIQUID")):
				incidentmbo.setValue("APAHSELIQUID",mbo.getBoolean("APAHSELIQUID"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getBoolean("APAHSEGASES") != mbo.getBoolean("APAHSEGASES")):
				incidentmbo.setValue("APAHSEGASES",mbo.getBoolean("APAHSEGASES"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			if (incidentmbo.getBoolean("APAHSESOLIDS") != mbo.getBoolean("APAHSESOLIDS")):
				incidentmbo.setValue("APAHSESOLIDS",mbo.getBoolean("APAHSESOLIDS"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSELIQUIDVAL") != mbo.getString("APAHSELIQUIDVAL")):
				incidentmbo.setValue("APAHSELIQUIDVAL",mbo.getString("APAHSELIQUIDVAL"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSEGASESVAL") != mbo.getString("APAHSEGASESVAL")):
				incidentmbo.setValue("APAHSEGASESVAL",mbo.getString("APAHSEGASESVAL"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)	
			if (incidentmbo.getString("APAHSESOLIDSVAL") != mbo.getString("APAHSESOLIDSVAL")):
				incidentmbo.setValue("APAHSESOLIDSVAL",mbo.getString("APAHSESOLIDSVAL"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
			if (incidentmbo.getString("APAHSELOSSCONT") != mbo.getString("APAHSELOSSCONT")):
				incidentmbo.setValue("APAHSELOSSCONT",mbo.getString("APAHSELOSSCONT"),MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)			
	#DMND0002395 - Set APANONHSE value for Location filtering, we need to loop through and change the Action Tracking OIM flag when the Classification has changed
	if mbo.isModified("CLASSSTRUCTUREID"):
		previousClasstructure = mbo.getMboValue("CLASSSTRUCTUREID").getPreviousValue().asString()
		wasHSE = mbo.getMboSet("$classancestor","classancestor","ancestorclassid = 'OP' and classstructureid='" + previousClasstructure + "'").isEmpty()
		isHSE = mbo.getMboSet("$classancestor","classancestor","ancestorclassid = 'OP' and classstructureid=:classstructureid").isEmpty()
		if wasHSE != isHSE:
			actionMboSet = mbo.getMboSet("APAHSEPLUSGACT")
			if not actionMboSet.isEmpty():
				actionMbo = actionMboSet.moveFirst()
				while actionMbo is not None:
					if isHSE:
						actionMbo.setValue("APANONHSE", 0, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION)
					else:
						actionMbo.setValue("APANONHSE", 1, MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION)
					actionMbo = actionMboSet.moveNext()