#description=Set editability of Assurance Failure Additional Details. PROBLEM.APAHSEISASSFAIL
#language=jython
#log=DEBUG
#name=APAHSE_FLD_PROBLEM_APAHSEISASS
#status=Active
#launchpoint.1.active=1
#launchpoint.1.attribute=APAHSEISASSFAIL
#launchpoint.1.description=Set editability of Assurance Failure Additional Details. PROBLEM.APAHSEISASSFAIL
#launchpoint.1.name=APAHSE_FLD_PROBLEM_APAHSEISASS
#launchpoint.1.object=PROBLEM
#launchpoint.1.type=ATTRIBUTE
##

##########################################################
# Copyright IBM Corp. 2014
#
# APA HSE Project
#
# Script APAHSE_FLD_PROBLEM_APAHSEISASS
# Authors:	ddepaula@au1.ibm.com
##########################################################

# Script sets editability of the Assurance Failure Details section based on the value
# of the Assurance Failure flag. If flag is cleared, all fields in the section are also cleared.

from psdi.mbo import MboConstants

fields = ["APAHSEASSFAILDET","APAHSEASSFAILDET_LONGDESCRIPTION"]

if mbo.getBoolean("APAHSEISASSFAIL"):
	# if the flag is ticked, enable Assurance Failure Details fields
	mbo.setFieldFlag(fields,MboConstants.READONLY,False)
	mbo.setFieldFlag("APAHSEASSFAILDET",MboConstants.REQUIRED,True)
else:
	# if flag is cleared, disable all fields and clear values
	mbo.setValueNull("APAHSEASSFAILDET_LONGDESCRIPTION",MboConstants.NOACCESSCHECK)
	mbo.setValueNull("APAHSEASSFAILDET",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("APAHSEASSFAILDET",MboConstants.REQUIRED,False)
	mbo.setFieldFlag(fields,MboConstants.READONLY,True)