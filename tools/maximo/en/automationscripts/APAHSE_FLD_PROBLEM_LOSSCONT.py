##########################################################
# 
# APA HSE Investigations application Demand request.
# DMND0003495 - CMR 655 - Loss of Containment section in Investigations
#
# Script: APAHSE_FLD_PROBLEM_LOSSCONT 
# Author: Yugesh
#
##########################################################

from psdi.server import MXServer
from psdi.mbo import MboConstants	

foundLosCont = 0
# Automation Script to enable/disable fields based on the Loss of Containment section field selection
liquid = mbo.getBoolean("APAHSELIQUID")
gases = mbo.getBoolean("APAHSEGASES")
solids = mbo.getBoolean("APAHSESOLIDS")

def setLosSpecontReadOnlyFlags(mbo,flagValue):
  mbo.setFieldFlag("MEASUREUNITID",MboConstants.READONLY,flagValue)
  mbo.setFieldFlag("SECTION",MboConstants.READONLY,flagValue)
  mbo.setFieldFlag("NUMVALUE",MboConstants.READONLY,flagValue)
  mbo.setFieldFlag("TABLEVALUE",MboConstants.READONLY,flagValue)

ticketSpecSet = mbo.getMboSet("TICKETSPECCLASS")
isTicketSpecSetEmpty = ticketSpecSet.isEmpty()

if liquid or gases or solids:
	mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,False)
else:
	mbo.setValueNull("APAHSELOSSCONT",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("APAHSELOSSCONT",MboConstants.READONLY,True)
	
if launchPoint == "APAHSE_FLD_PROBLEM_LIQUID" or launchPoint == "APAHSE_FLD_INCIDENT_LIQUID":
	if liquid and gases:
		if isTicketSpecSetEmpty: 
			errorkey="ClassId"
			errorgroup="apahsecommon"
		if not isTicketSpecSetEmpty:
			for y in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(y)	
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CONT_GAS":
					foundLosCont = 1
					mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,False)
				if foundLosCont == 1 and ticketSpecMbo.toBeDeleted():
					ticketSpecMbo.undelete()
			if foundLosCont == 0:
				ticketSpecMbo = ticketSpecSet.add()
				ticketSpecMbo.setValue("ASSETATTRID","LOS_CONT_GAS",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				setLosSpecontReadOnlyFlags(ticketSpecMbo,True)
				mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,False)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	elif liquid:
		if isTicketSpecSetEmpty: 
			errorkey="ClassId"
			errorgroup="apahsecommon"
		if not isTicketSpecSetEmpty:
			for x in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(x)		
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CON_TIER":
					foundLosCont = 1
					mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,False)
				if foundLosCont == 1 and ticketSpecMbo.toBeDeleted():
					ticketSpecMbo.undelete()
			if foundLosCont == 0:
				ticketSpecMbo = ticketSpecSet.add()
				ticketSpecMbo.setValue("ASSETATTRID","LOS_CON_TIER",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				setLosSpecontReadOnlyFlags(ticketSpecMbo,True)
				mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,False)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	elif gases and not liquid:
		if not isTicketSpecSetEmpty:	
			for z in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(z)			 
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CONT_GAS":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)	
		mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSELIQUIDVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	else:
		if not isTicketSpecSetEmpty and not (solids or gases):	
			for z in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(z)			 
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CON_TIER":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)	
		mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSELIQUIDVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSELIQUIDVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
		
if launchPoint == "APAHSE_FLD_PROBLEM_GASES" or launchPoint == "APAHSE_FLD_INCIDENT_GASES":
	if gases and (liquid or solids):
		if isTicketSpecSetEmpty: 
			errorkey="ClassId"
			errorgroup="apahsecommon"
		if not isTicketSpecSetEmpty:
			for c in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(c)	
				if ticketSpecMbo.getString("ASSETATTRID").upper() == "LOS_CONT_GAS":
					foundLosCont = 1
					mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,False)
				if foundLosCont == 1 and ticketSpecMbo.toBeDeleted():
					ticketSpecMbo.undelete()
			if foundLosCont == 0:
				ticketSpecMbo = ticketSpecSet.add()
				ticketSpecMbo.setValue("ASSETATTRID","LOS_CONT_GAS",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				setLosSpecontReadOnlyFlags(ticketSpecMbo,True)
				mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,False)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	elif gases:
		if isTicketSpecSetEmpty: 
			errorkey="ClassId"
			errorgroup="apahsecommon"
		if not isTicketSpecSetEmpty:	
			for a in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(a)	
				if ticketSpecMbo.getString("ASSETATTRID").upper() == "LOS_CON_TIER":
					foundLosCont = 1
					mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,False)
				if foundLosCont == 1 and ticketSpecMbo.toBeDeleted():
					ticketSpecMbo.undelete()
			if foundLosCont == 0:
				ticketSpecMbo = ticketSpecSet.add()
				ticketSpecMbo.setValue("ASSETATTRID","LOS_CON_TIER",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				setLosSpecontReadOnlyFlags(ticketSpecMbo,True)
				mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,False)
	elif gases and not (solids or liquid):
		if not isTicketSpecSetEmpty:	
			for z in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(z)			 
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CONT_GAS":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)	
		mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEGASESVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	elif (solids or liquid) and not gases:
		if not isTicketSpecSetEmpty:	
			for z in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(z)			 
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CONT_GAS":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)	
		mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEGASESVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	else:
		if not isTicketSpecSetEmpty and not (solids or liquid):
			for b in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(b)			
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CON_TIER":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)			
		mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEGASESVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEGASESVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
		
if launchPoint == "APAHSE_FLD_PROBLEM_SOLIDS" or launchPoint == "APAHSE_FLD_INCIDENT_SOLIDS":					
	if solids and gases:
		if isTicketSpecSetEmpty: 
			errorkey="ClassId"
			errorgroup="apahsecommon"
		if not isTicketSpecSetEmpty:
			for j in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(j)	
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CONT_GAS":
					foundLosCont = 1
					mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,False)
				if foundLosCont == 1 and ticketSpecMbo.toBeDeleted():
					ticketSpecMbo.undelete()
			if foundLosCont == 0:
				ticketSpecMbo = ticketSpecSet.add()
				ticketSpecMbo.setValue("ASSETATTRID","LOS_CONT_GAS",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				setLosSpecontReadOnlyFlags(ticketSpecMbo,True)
				mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,False)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	elif solids:
		if isTicketSpecSetEmpty: 
			errorkey="ClassId"
			errorgroup="apahsecommon"
		if not isTicketSpecSetEmpty:
			for i in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(i)	
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CON_TIER":
					foundLosCont = 1
					mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,True)
					mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,False)
				if foundLosCont == 1 and ticketSpecMbo.toBeDeleted():
					ticketSpecMbo.undelete()
			if foundLosCont == 0:
				ticketSpecMbo = ticketSpecSet.add()
				ticketSpecMbo.setValue("ASSETATTRID","LOS_CON_TIER",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
				setLosSpecontReadOnlyFlags(ticketSpecMbo,True)
				mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,True)
				mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,False)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	elif gases and not solids:
		if not isTicketSpecSetEmpty:	
			for z in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(z)			 
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CONT_GAS":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)	
		mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSESOLIDSVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())
	else:
		if not isTicketSpecSetEmpty and not (liquid or gases):	
			for k in range(0, ticketSpecSet.count()):
				ticketSpecMbo = ticketSpecSet.getMbo(k)			 
				if ticketSpecMbo.getString("ASSETATTRID") == "LOS_CON_TIER":
					ticketSpecMbo.delete(MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSESOLIDSVAL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSESOLIDSVAL",MboConstants.READONLY,True)
		try:
			MXServer.getBulletinBoard().post("$updateingIncTypeFromScript$",mbo.getUserInfo())
		finally:
			# remove the message
			MXServer.getBulletinBoard().remove("$updateingIncTypeFromScript$",mbo.getUserInfo())