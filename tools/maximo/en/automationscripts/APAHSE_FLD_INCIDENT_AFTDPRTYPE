#description=Automation Script associated to the Person Affected Is field on the incident. APAHSEAFFECTEDPRSTYPE.
#language=jython
#log=DEBUG
#name=APAHSE_FLD_INCIDENT_AFTDPRTYPE
#status=Active
#launchpoint.1.active=1
#launchpoint.1.attribute=APAHSEAFFECTEDPRSTYPE
#launchpoint.1.description=Automation Script associated to the Person Affected Is field on an incident. APAHSEAFFECTEDPRSTYPE
#launchpoint.1.name=APAHSE_FLD_INCIDENT_AFTDPRTYPE
#launchpoint.1.object=INCIDENT
#launchpoint.1.type=ATTRIBUTE
##

##########################################################
# Copyright IBM Corp. 2014
#
# APA HSE Project
#
# Script APAHSE_FLD_INCIDENT_AFTDPRTYPE
# Author:acherem@au1.ibm.com
##########################################################

from psdi.mbo import MboConstants

afType = mbo.getString("APAHSEAFFECTEDPRSTYPE")

if len(afType)>0 and app == "APAHSEINC":
	if afType == "EMPLOYEE":
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,False)
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,True)
		
		#clean up other fields
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
		mbo.setValueNull("VENDOR",MboConstants.NOACCESSCHECK)
		
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,False)	
		mbo.setValueNull("APAHSECONTRACTORNAME",MboConstants.NOACCESSCHECK)
		
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,False)	
		mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK)
		
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,False)
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
		
		mbo.setValueNull("AFFECTEDPHONE",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,False)
		mbo.setValueNull("AFFECTEDEMAIL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,False)
	elif afType in("CONTRACT","SUBCONT"):
		#clean up id
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,False)
		mbo.setValueNull("AFFECTEDPERSONID",MboConstants.NOACCESSCHECK)
		
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,False)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,True)	
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,True)	
		mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,False)
		mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,False)
	elif afType in("VISITOR","PUBLIC"):
		#clean up id
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,False)
		mbo.setValueNull("AFFECTEDPERSONID",MboConstants.NOACCESSCHECK)
		
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
		mbo.setValueNull("VENDOR",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
		mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)	
		mbo.setValueNull("AFFECTEDUSERNAME",MboConstants.NOACCESSCHECK)
		
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,True)	
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,False)	
		mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,False)
		mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,False)
elif len(afType)>0 and app == "APAHSEHR":
	if afType == "EMPLOYEE":
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("APAHSEONBEHALFOFNAME",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK)
	elif afType in("CONTRACT","SUBCONT"):
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,False)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSEONBEHALFOFNAME",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,True)	
	elif afType in("VISITOR","PUBLIC"):
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
		mbo.setValueNull("VENDOR",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEONBEHALFOFNAME",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,False)	
		mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK)
	else:
		mbo.setValueNull("VENDOR",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEONBEHALFOFNAME",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEONBEHALFOFNAME",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,False)	
		mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK)
else:
	# clean up fields
	mbo.setValueNull("AFFECTEDPERSONID",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.REQUIRED,False)
	mbo.setValueNull("AFFECTEDUSERNAME",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.REQUIRED,False)
	mbo.setValueNull("VENDOR",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("VENDOR",MboConstants.REQUIRED,False)
	mbo.setValueNull("AFFECTEDPHONE",MboConstants.NOACCESSCHECK)
	mbo.setValueNull("AFFECTEDEMAIL",MboConstants.NOACCESSCHECK)
	mbo.setValueNull("APAHSECONTRACTORNAME",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.REQUIRED,False)
	mbo.setValueNull("APAHSEPRINCIPLECONTRACTOR",MboConstants.NOACCESSCHECK)
	mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.REQUIRED,False)
	mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)		
	mbo.setFieldFlag("AFFECTEDPERSONID",MboConstants.READONLY,True)
	mbo.setFieldFlag("AFFECTEDUSERNAME",MboConstants.READONLY,True)
	mbo.setFieldFlag("VENDOR",MboConstants.READONLY,True)
	mbo.setFieldFlag("AFFECTEDPHONE",MboConstants.READONLY,True)
	mbo.setFieldFlag("AFFECTEDEMAIL",MboConstants.READONLY,True)
	mbo.setFieldFlag("APAHSECONTRACTORNAME",MboConstants.READONLY,True)
	mbo.setFieldFlag("APAHSEPRINCIPLECONTRACTOR",MboConstants.READONLY,True)
	
	mbo.setFieldFlag("APAHSEONBEHALFOFNAME",MboConstants.REQUIRED,False)
	mbo.setValueNull("APAHSEONBEHALFOFNAME",MboConstants.NOACCESSCHECK)