#description=Automation Script to enable/disable fields based on the Asset Strike field
#language=jython
#log=ERROR
#name=APAHSE_FLD_INCIDENT_ASSETSTR
#status=Active
#launchpoint.1.active=1
#launchpoint.1.attribute=APAHSEASSETSTRIKE
#launchpoint.1.description=Launchpoint to enable/disable fields based on the Asset Strike field
#launchpoint.1.name=APAHSE_FLD_INCIDENT_ASSETSTR
#launchpoint.1.object=INCIDENT
#launchpoint.1.type=ATTRIBUTE
#launchpoint.2.active=1
#launchpoint.2.attribute=APAHSESTRIKEELEC
#launchpoint.2.description=Launchpoint to enable/disable fields based on the Electric field
#launchpoint.2.name=APAHSE_FLD_INCIDENT_ELECTRIC
#launchpoint.2.object=INCIDENT
#launchpoint.2.type=ATTRIBUTE
#launchpoint.3.active=1
#launchpoint.3.attribute=APAHSESTRIKEGAS
#launchpoint.3.description=Launchpoint to enable/disable fields based on the gas field
#launchpoint.3.name=APAHSE_FLD_INCIDENT_GAS
#launchpoint.3.object=INCIDENT
#launchpoint.3.type=ATTRIBUTE
#launchpoint.4.active=1
#launchpoint.4.attribute=APAHSEASSETSTRIKE
#launchpoint.4.description=Launchpoint to enable/disable fields based on the Asset Strike field
#launchpoint.4.name=APAHSE_FLD_PROBLEM_ASSETSTR
#launchpoint.4.object=PROBLEM
#launchpoint.4.type=ATTRIBUTE
#launchpoint.5.active=1
#launchpoint.5.attribute=APAHSESTRIKEELEC
#launchpoint.5.description=Launchpoint to enable/disable fields based on the Electric field
#launchpoint.5.name=APAHSE_FLD_PROBLEM_ELECTRIC
#launchpoint.5.object=PROBLEM
#launchpoint.5.type=ATTRIBUTE
#launchpoint.6.active=1
#launchpoint.6.attribute=APAHSESTRIKEGAS
#launchpoint.6.description=Launchpoint to enable/disable fields based on the gas field
#launchpoint.6.name=APAHSE_FLD_PROBLEM_GAS
#launchpoint.6.object=PROBLEM
#launchpoint.6.type=ATTRIBUTE
##

##########################################################
# Copyright IBM Corp. 2016
#
# APA HSE Project
#
# Script APAHSE_FLD_INCIDENT_ASSETSTR
# Author:mgiang@au1.ibm.com
##########################################################

from psdi.server import MXServer

# Automation Script to enable/disable fields based on the Asset Strike field
from psdi.mbo import MboConstants
if launchPoint == "APAHSE_FLD_INCIDENT_ASSETSTR" or launchPoint == "APAHSE_FLD_PROBLEM_ASSETSTR":
	assetStrike = mbo.getBoolean("APAHSEASSETSTRIKE");
	if not assetStrike:
		mbo.setValue("APAHSESTRIKEGAS", False)
		mbo.setValue("APAHSESTRIKEELEC",False)
		mbo.setValue("APAHSESTRIKETEL",False)
		mbo.setValue("APAHSESTRIKEWAT",False)
		mbo.setValue("APAHSESTRIKESEW", False)
		mbo.setValue("APAHSESTRIKEOTH",False)
		mbo.setValueNull("APAHSESTRIKEDETAIL",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEELECVOLT",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEGASPRESS",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEDEVICEUSED",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.REQUIRED,False)
		mbo.setFieldFlag("APAHSESTRIKEGAS",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSESTRIKEELEC",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSESTRIKETEL",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSESTRIKEWAT",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSESTRIKESEW",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSESTRIKEOTH",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
		mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.READONLY,True)
	else:
		mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSESTRIKEGAS",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSESTRIKEELEC",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSESTRIKETEL",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSESTRIKEWAT",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSESTRIKESEW",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSESTRIKEOTH",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSESTRIKEDETAIL",MboConstants.READONLY,False)
		mbo.setFieldFlag("APAHSEDEVICEUSED",MboConstants.READONLY,False)
if launchPoint == "APAHSE_FLD_INCIDENT_ELECTRIC" or launchPoint == "APAHSE_FLD_PROBLEM_ELECTRIC":
	if mbo.getBoolean("APAHSESTRIKEELEC"):
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,False)
	else:
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEELECVOLT",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEELECVOLT",MboConstants.READONLY,True)
if launchPoint == "APAHSE_FLD_INCIDENT_GAS" or launchPoint == "APAHSE_FLD_PROBLEM_GAS":
	if mbo.getBoolean("APAHSESTRIKEGAS"):
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.REQUIRED,True)
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,False)
	else:
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.REQUIRED,False)
		mbo.setValueNull("APAHSEGASPRESS",MboConstants.NOACCESSCHECK)
		mbo.setFieldFlag("APAHSEGASPRESS",MboConstants.READONLY,True)
