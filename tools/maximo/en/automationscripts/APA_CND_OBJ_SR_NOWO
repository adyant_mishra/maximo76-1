#description=.This script creates a validates whether a duplicate WO record already exists for the SR
#language=jython
#log=ERROR
#name=APA_CND_OBJ_SR_NOWO
#status=Active
#launchpoint.1.active=1
#launchpoint.1.description=Launchpoint to check if a duplicate Work order already exists for the SR
#launchpoint.1.name=APA_CND_SR_WO
#launchpoint.1.object=SR
#launchpoint.1.type=CUSTOMCONDITION
##

##################################################################################
#
# Script APA_CND_OBJ_SR_NOWO
#
# Author: mgiang@au1.ibm.com
#
# DMND0002336 - This automation script is used to evaluate a Workflow condition for APA_SRVAL.
# It verifies that the SM-012 rule is active, and checks to see if there are any duplicate
# Work Order already existing in the System, it also generates a Work Log record
##################################################################################
from psdi.server import MXServer
from psdi.util.logging import MXLoggerFactory
from psdi.mbo import MboConstants

logger = MXLoggerFactory.getLogger("maximo.script")
logger.debug("In APA_CND_OBJ_SR_NOWO for SR: " + mbo.getString("TICKETID"))

evalresult = True
ruleSM012mboSet = mbo.getMboSet("APA_SRRM_DUPLICATEWO")
targstartdate = mbo.getString("TARGETSTART")
if ruleSM012mboSet.isEmpty() or targstartdate == "":
	#Rule has not been defined, or target start date not defined, so we let the WF progress
	evalresult = True
	logger.debug("In APA_CND_OBJ_SR_NOWO SM012 rule not found or start date not entered")
else:
	ruleSM012Mbo = ruleSM012mboSet.getMbo(0)
	checkDuplicate = ruleSM012Mbo.getString("OUTPUT1")
	if checkDuplicate in ('N','n','0'):
		#Rule has intentionally been turned off
		evalresult = True
		logger.debug("In APA_CND_OBJ_SR_NOWO SM012 rule found but turned off")
	elif checkDuplicate in ('Y','y','1'):
		logger.debug("In APA_CND_OBJ_SR_NOWO SM012 rule found and turned on")
		beforeTargetRange = ruleSM012Mbo.getInt("INPUT3")
		afterTargetRange = ruleSM012Mbo.getInt("INPUT4")
		logger.debug("In APA_CND_OBJ_SR_NOWO before target start date days:" + str(beforeTargetRange))
		logger.debug("In APA_CND_OBJ_SR_NOWO after target start date days:" + str(afterTargetRange))
		# Get Work Order Auto Creation Rule
		ruleSM004mboSet = mbo.getMboSet("APA_SRRM_WOAC")
		if ruleSM004mboSet.isEmpty():
			#Rule has not been defined so the SR does not map to a Work Order
			evalresult = True
			logger.debug("In APA_CND_OBJ_SR_NOWO SM004 rule not found")
		else:
			ruleSM004Mbo = ruleSM004mboSet.getMbo(0)
			woClassification = ruleSM004Mbo.getString("OUTPUT1")
			workorder = ruleSM004Mbo.getString("OUTPUT2")
			logger.debug("In APA_CND_OBJ_SR_NOWO SM004 rule outputs:" + woClassification + "and :" + workorder)
			if workorder == 'WORKORDER':
				whereString = "historyflag  =  0  and woclass  =  'WORKORDER' and istask  =  0 "
				whereString += " and siteid  =  'APA'  and status in ('APPR','FLDCOMP','WAPPR') and wonum not like 'A%' and targstartdate between "
				whereString += ":TARGETSTART - TO_NUMBER(" + str(beforeTargetRange) + ")  and :TARGETSTART + TO_NUMBER("
				whereString += str(afterTargetRange) + ") And classstructureid in (select CLASSSTRUCTUREID from CLASSANCESTOR where ancestorclassid = " 
				whereString += "'" + woClassification + "') and location =:location"
				logger.debug("In APA_CND_OBJ_SR_NOWO whereString:" + whereString)
				workOrderSet = mbo.getMboSet("$workorder","WORKORDER", whereString)
				if not workOrderSet.isEmpty():
					evalresult = False
					wonum = workOrderSet.getMbo(0).getString("WONUM")
					logger.debug("In APA_CND_OBJ_SR_NOWO found a Work Order:" + wonum)
					description="Verify Service Request, potential duplicate Work Order stopped "
					longdescription="There is another Work Order :" + wonum + " which has already been Approved on the same location and with a matching Target Start Date. Please review and assess this Service Request."
					worklogset = mbo.getMboSet("MODIFYWORKLOG")
					worklog = worklogset.add()
					if worklog is not None:
						worklog.setValue("DESCRIPTION",description)
						worklog.setValue("DESCRIPTION_LONGDESCRIPTION",longdescription)
						worklog.setValue("CREATEBY","MAXADMIN",MboConstants.NOACCESSCHECK | MboConstants.NOVALIDATION_AND_NOACTION)
