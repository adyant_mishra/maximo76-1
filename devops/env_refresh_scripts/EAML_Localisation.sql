---EAML Refresh Script
-- 06-Oct-17 treoxl (created for 7.6 LOCALDEV)
-- 03-Nov-17 treoxl (mod for 7.6 DEV)
update maximo.maxendpointdtl set value = 'https://dc2ap871.pipelinetrust.com.au/APA.Maximo.PublishAPA_WOOUT/APA_Maximo_Orchestration_ReceivePublishWorkOrder_PublishAPA_WOOUTPort.svc' where endpointname in 'APA_BIZTALK_WO' and property = 'ENDPOINTURL';
update maximo.maxendpointdtl set value = 'Publish' where endpointname in 'APA_BIZTALK_WO' and property = 'SOAPACTION';
update maximo.maxendpointdtl set value = 'sendrobust' where endpointname in 'APA_BIZTALK_WO' and property = 'MEP';
update maximo.maxendpointdtl set value = 'BizTalkServiceInstance' where endpointname in 'APA_BIZTALK_WO' and property = 'SERVICENAME';
update maximo.maxendpointdtl set value = 'PIPELINETRUST\svcBTMaximoDEV' where endpointname in 'APA_BIZTALK_WO' and property = 'USERNAME';
update maximo.maxendpointdtl set password = null where endpointname in 'APA_BIZTALK_WO' and property = 'PASSWORD';

update maximo.maxendpointdtl set value = 'https://dc2ap871.pipelinetrust.com.au/APA.Maximo.PublishAPA_SROUT/APA_Maximo_Orchestration_ReceivePublishServiceRequest_PublishAPA_SROUTPort.svc' where endpointname in 'APA_BIZTALK_SR' and property = 'ENDPOINTURL';
update maximo.maxendpointdtl set value = 'Publish' where endpointname in 'APA_BIZTALK_SR' and property = 'SOAPACTION';
update maximo.maxendpointdtl set value = 'sendrobust' where endpointname in 'APA_BIZTALK_SR' and property = 'MEP';
update maximo.maxendpointdtl set value = 'BizTalkServiceInstance' where endpointname in 'APA_BIZTALK_SR' and property = 'SERVICENAME';
update maximo.maxendpointdtl set value = 'PIPELINETRUST\svcBTMaximoDEV' where endpointname in 'APA_BIZTALK_SR' and property = 'USERNAME';
update maximo.maxendpointdtl set password = null where endpointname in 'APA_BIZTALK_SR' and property = 'PASSWORD';

update maximo.maxendpointdtl set value = 'https://dc2ap871.pipelinetrust.com.au/APA.Maximo.PublishAPA_LOCOUT/APA_Maximo_Orchestration_ReceivePublishLocation_PublishAPA_LOCOUTPort.svc' where endpointname in 'APA_BIZTALK_LOC' and property = 'ENDPOINTURL';
update maximo.maxendpointdtl set value = 'Publish' where endpointname in 'APA_BIZTALK_LOC' and property = 'SOAPACTION';
update maximo.maxendpointdtl set value = 'sendrobust' where endpointname in 'APA_BIZTALK_LOC' and property = 'MEP';
update maximo.maxendpointdtl set value = 'BizTalkServiceInstance' where endpointname in 'APA_BIZTALK_LOC' and property = 'SERVICENAME';
update maximo.maxendpointdtl set value = 'PIPELINETRUST\svcBTMaximoDEV' where endpointname in 'APA_BIZTALK_LOC' and property = 'USERNAME';
update maximo.maxendpointdtl set password = null where endpointname in 'APA_BIZTALK_LOC' and property = 'PASSWORD';

update maximo.maxendpointdtl set value = 'https://dc2ap871.pipelinetrust.com.au/APA.Maximo.PublishAPA_SAOUT/APA_Maximo_Orchestration_ReceivePublishServiceAddress_PublishAPA_SAOUTPort.svc' where endpointname in 'APA_BIZTALK_SA' and property = 'ENDPOINTURL';
update maximo.maxendpointdtl set value = 'Publish' where endpointname in 'APA_BIZTALK_SA' and property = 'SOAPACTION';
update maximo.maxendpointdtl set value = 'sendrobust' where endpointname in 'APA_BIZTALK_SA' and property = 'MEP';
update maximo.maxendpointdtl set value = 'BizTalkServiceInstance' where endpointname in 'APA_BIZTALK_SA' and property = 'SERVICENAME';
update maximo.maxendpointdtl set value = 'PIPELINETRUST\svcBTMaximoDEV' where endpointname in 'APA_BIZTALK_SA' and property = 'USERNAME';
update maximo.maxendpointdtl set password = null where endpointname in 'APA_BIZTALK_SA' and property = 'PASSWORD';
update maximo.maxendpointdtl set value = 'oracle.jdbc.OracleDriver' where endpointname in 'APA_OA12' and property = 'DRIVER';
update maximo.maxendpointdtl set value = '1' where endpointname in 'APA_OA12' and property = 'ISREMOTE';
update maximo.maxendpointdtl set value = 'jdbc:oracle:thin:@dc2rn201-v:1591/FING' where endpointname in 'APA_OA12' and property = 'URL';
update maximo.maxendpointdtl set value = 'MAXORA' where endpointname in 'APA_OA12' and property = 'USERNAME';
update maximo.maxendpointdtl set password = null where endpointname in 'APA_OA12' and property = 'PASSWORD';

update maximo.maxpropvalue set propvalue = 'https://maximodev.pipelinetrust.com.au/meaweb' where propname = 'mxe.int.webappurl';
update maximo.maxpropvalue set propvalue = '\\maximofileshare\MaximoDEV\meaglobaldir' where propname = 'mxe.int.globaldir';
update maximo.maxpropvalue set propvalue = 'admin@dc2ap871.pipelinetrust.com.au' where propname = 'mxe.adminEmail';
update maximo.maxpropvalue set propvalue = '\\maximofileshare\MaximoDEV\DOCLINKS' where propname = 'mxe.doclink.doctypes.defpath';
update maximo.maxpropvalue set propvalue = '\\maximofileshare\MaximoDEV=https://maximodev.pipelinetrust.com.au:9600' where propname = 'mxe.doclink.path01';
update maximo.maxpropvalue set propvalue = '\\maximofileshare\MaximoDEV\DOCLINKS' where propname = 'mxe.doclink.doctypes.topLevelPaths';
update maximo.maxpropvalue set propvalue = 'maximodev.pipelinetrust.com.au' where propname = 'mxe.hostname';
update maximo.maxpropvalue set propvalue = 'maximodev.pipelinetrust.com.au' where propname = 'mxe.help.host';
update maximo.maxpropvalue set propvalue = 'https' where propname = 'mxe.help.protocol';
update maximo.maxpropvalue set propvalue = '443' where propname = 'mxe.help.port';
update maximo.maxpropvalue set propvalue = '1' where propname = 'mxe.useAppServerSecurity';
update maximo.maxpropvalue set propvalue = 'gissauat.pipelinetrust.com.au' where propname = 'apa.sias.server.sa';
update maximo.maxpropvalue set propvalue = 'gisvicuat.pipelinetrust.com.au' where propname = 'apa.sias.server.vic';
update maximo.maxpropvalue set propvalue = 'ias_maximo_uat_sa_proxy' where propname = 'apa.sias.context.sa';
update maximo.maxpropvalue set propvalue = 'ias_maximo_uat_vic_proxy' where propname = 'apa.sias.context.vic';
update maximo.maxpropvalue set propvalue = 'dc2ap871.pipelinetrust.com.au' where propname = 'mail.smtp.host';
update maximo.maxpropvalue set propvalue = 'MaxEmail@dc2ap871.pipelinetrust.com.au' where propname = 'emailaddress';
update maximo.maxpropvalue set propvalue = 'dc2ap871.pipelinetrust.com.au' where propname = 'mailserver';
update maximo.maxpropvalue set propvalue = '110' where propname = 'port';
update maximo.maxpropvalue set propvalue = 'INBOX' where propname = 'emailfolder';
update maximo.maxpropvalue set propvalue = 'B7B311284F1FAB0FC561929312835263' where propname = 'emailpassword';
update maximo.maxpropvalue set propvalue = 'MaximoAdmin@dc2ap871.pipelinetrust.com.au' where propname = 'adminemail';


update maximo.inbcommsecurity set emailaddress = 'MaxEmail@dc2ap871.pipelinetrust.com.au' where app = 'PO';
update maximo.inbcommsecurity set mailserver = 'dc2ap871.pipelinetrust.com.au' where app = 'PO';
update maximo.inbcommsecurity set emailaddress = 'MaxEmail@dc2ap871.pipelinetrust.com.au' where app = 'PLUSGPO';
update maximo.inbcommsecurity set mailserver = 'dc2ap871.pipelinetrust.com.au' where app = 'PLUSGPO';
update maximo.inbcommsecurity set emailaddress = 'MaxEmail@dc2ap871.pipelinetrust.com.au' where app = 'SR';
update maximo.inbcommsecurity set mailserver = 'dc2ap871.pipelinetrust.com.au' where app = 'SR';


alter user readonly identified by readonly;
alter user datarec identified by datarec;

truncate table maxinterror;
truncate table maxinterrormsg;
truncate table MAXINTERROREXTRACT;
truncate table maxintmsgtrk;
truncate table maxintmsgtrkdtl;
truncate table taskscheduler;

update maximo.maxmessages set value = 'Welcome to Maximo (DEV) Refreshed on ' || trunc(sysdate) where msgkey = 'welcomemaximomessage';
update maximo.maxapps set description = description || ' (DEV)' WHERE apptype = 'RUN'; 
update maximo.maxpropinstance set servername = replace(servername,'maxprd','maxld') where 1=1;
update maximo.maxpropvalue set servername = replace(servername,'maxprd','maxld') where 1=1;
update maximo.maxuser SET loginid = 'maxadmin' WHERE UPPER(loginid) = 'MAXADMIN';
update maximo.maxuser SET loginid = 'mxintadmuat' WHERE UPPER(loginid) = 'MXINTADM';

UPDATE maxuser SET password = '57CE0489ED835F0C904B4E9434DB2981' WHERE loginid = 'maxadmin';  -- pasword "maximo1"

--update maximo.maxuser set loginid = lower(loginid) where userid not like '1%' and lower(loginid) not in 
--(select lower(loginid)from maxuser group by lower(loginid) having count(*) > 1);
--UPDATE maxuser SET PASSWORD='57CE0489ED835F0C904B4E9434DB2981' where 1=1;

--update maximo.email set emailaddress = replace(emailaddress,'@','@adlap961.') where emailaddress not like '%@dc2ap809.%';

update maximo.email
   set emailaddress = replace(emailaddress, '@', '@dc2ap871.')
 where emailaddress not like '%@dc2ap871.%'
   and replace(emailaddress, '@', '@dc2ap871.') not in
       (select emailaddress from email);


-- DISABLE UNWANTED CRONTASKS
update maximo.crontaskinstance set active = 0
where crontaskname in ('REPORTSCHEDULE','LDAPSYNC','PMWoGenCronTask','PMForecastCronTask');


DROP INDEX SIB000STREAMIX;
DROP TABLE SIB000;
DROP INDEX SIB001STREAMIX;
DROP TABLE SIB001;
DROP INDEX SIB002STREAMIX;
DROP TABLE SIB002;
DROP TABLE SIBCLASSMAP;
DROP TABLE SIBKEYS;
DROP TABLE SIBLISTING;
DROP TABLE SIBOWNER;
DROP TABLE SIBOWNERO;
DROP TABLE SIBXACTS;
--LOG IN AS DATAREC, DISABLE DATAREC DBMSJOB #23 AS IT WILL FAIL DUE TO REMOVED DBLINKS.

--CLEAR EVENT RESPONSE
create table BKUPEVENTRESPONSE as select * from eventresponse;
delete from eventresponse where 1=1;

select count(*) from BKUPEVENTRESPONSE;
select count(*) from eventresponse;
select count(*), sourcetable from eventresponse group by sourcetable;

--DELETE SENSITIVE DATA
delete from incident where apahsesensitive=1;

--CLEAR STUCK AUTOACCEPTS
select count(*), APP from maximo.wfassignment where assigncode = 'MAXADMIN' and assignstatus = 'ACTIVE' group by app;
update maximo.wfassignment set assignstatus = 'INACTIVE' where assigncode = 'MAXADMIN' and assignstatus = 'ACTIVE';

-- ****** ONLY RUN THIS BIT WHEN EVERYTHING ABOVE IS RUN AND VERIFIED *******
alter user readonly identified by eamlread2016;
alter user datarec identified by eamldata;

-- **************************************************************************
--Report Admin > Data Source
--May need to update password manually as it's encrypted
update REPORTDSPARAM set url = 'jdbc:oracle:thin:@(DESCRIPTION = (CONNECT_TIMEOUT = 35)(TRANSPORT_CONNECT_TIMEOUT = 30)(RETRY_COUNT = 3)(ADDRESS_LIST = (LOAD_BALANCE = on)(ADDRESS = (PROTOCOL = TCP)(HOST = dc2rc201scn)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = EAML)))' ;



